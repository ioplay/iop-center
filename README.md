# iop-center

Create your own individual media player controlled by physical inputs without coding.

iop-center provides the building blocks and a framework to easily use input and output devices in combination with the power of embedded computing.
Such devices may be switches, LED, motors, RFID readers and many more.
They can be used in any combination together with sound, music, slide shows or videos.


## Building blocks

The building blocks of iop-center are events and components.
Components are connected by events, they can send events and they can receive events.
The components provide the functionality.
There can be all kinds of components: detecting RFID cards, playing music or video, listening to buttons, selecting files to play depending on an input, and many more.


## Framework

The framework allows to build an application by selecting components and connecting them together.
Then, the framework takes care of the life cycle of the components.
All this can be done without coding.


## Usages

### RFID player

Playing music should be controlled with RFID cards.
Placing a card on the reader should play a song or album.
Placing another card plays a different album.
Removing the card stops the music.

This is realized with an RFID reader component that sends the detected card to a matching component.
The matching component selects an album depending on the detected card id and sends a playlist to a player component.
This player components then plays the music.

