/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "core/base/Component.h"
#include <map>
#include <queue>

namespace features_adapter
{


class EventRecorder :
    public core::base::Component
{
public:
  void recvEvent(const event::SharedEvent&) override;

  event::SharedEvent pop(const std::string& instanceName);
  std::size_t eventCount(const std::string& instanceName) const;
  void clearEvents();

private:
  std::map<std::string, std::queue<event::SharedEvent>> events{};

};


}
