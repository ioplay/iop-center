/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "EventRecorder.h"
#include "core/component/mpd/simulated/BufferSocketFactory.h"
#include "core/util/simulated/Filesystem.h"
#include "core/main/ComponentControl.h"
#include "core/base/Component.h"
#include "core/thread/MainThread.h"
#include "core/setup/fromCmdline.h"
#include <QCoreApplication>
#include <sstream>

namespace features_adapter
{


class Context
{
public:
  Context()
  {
    QObject::connect(&app, &core::main::ComponentControl::bootComplete, [this]{
      appStageChangeCompleted = true;
      isShutdown = false;
    });
    QObject::connect(&app, &core::main::ComponentControl::shutdownComplete, [this]{
      appStageChangeCompleted = true;
      isShutdown = true;
    });
  }

  int argc{1};
  char* argv[1]{"-test-"};
  QCoreApplication qapp{argc, argv};

  core::component::mpd::simulated::BufferSocketFactory socketFactory{};
  core::util::simulated::Filesystem fs{};
  std::stringstream cout{};

  EventRecorder receiver{};
  core::main::ComponentControl app{};
  core::thread::MainThread threadFactory{};

  std::optional<core::setup::Arguments> arguments{};

  bool appStageChangeCompleted{false};
  bool isShutdown{true};
};


}
