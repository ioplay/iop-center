/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "core/component/rfid/simulated/SimRegisterDevice.h"
#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>

namespace features_adapter::mfrc522
{
namespace
{


core::component::rfid::simulated::SimRegisterDevice::Registers parseRegisters(const ::cucumber::internal::Table& table)
{
  core::component::rfid::simulated::SimRegisterDevice::Registers registers{};

  for (const auto& row : table.hashes()) {
    const auto reg = std::stoul(row.at("register"), nullptr, 16);

    std::vector<uint8_t> values{};
    std::stringstream ss{row.at("value")};
    while (!ss.eof()) {
      unsigned val;
      ss >> std::hex >> val;
      values.push_back(val);
    }
    registers[reg] = values;
  }

  return registers;
}


GIVEN("^the MFRC522 device can not be opened$")
{
  cucumber::ScenarioScope<core::component::rfid::simulated::SimRegisterDevice> mfrc522;

  mfrc522->isOpenResult = false;
}

GIVEN("^the MFRC522 has the tx register values:$")
{
  TABLE_PARAM(regtable);

  cucumber::ScenarioScope<core::component::rfid::simulated::SimRegisterDevice> mfrc522;
  mfrc522->txRegisters = parseRegisters(regtable);
}

GIVEN("^I clear the MFRC522 rx registers$")
{
  cucumber::ScenarioScope<core::component::rfid::simulated::SimRegisterDevice> mfrc522;
  mfrc522->rxRegisters.clear();
}

THEN("^I expect the following MFRC522 rx register values:$")
{
  TABLE_PARAM(regtable);
  const auto expected = parseRegisters(regtable);

  cucumber::ScenarioScope<core::component::rfid::simulated::SimRegisterDevice> mfrc522;
  ASSERT_EQ(expected, mfrc522->rxRegisters);
}


}
}
