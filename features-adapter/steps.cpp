/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Context.h"
#include "Factories.h"
#include "core/component/mpd/SocketFactory.h"
#include "core/component/mpd/simulated/BufferSocket.h"
#include "core/setup/Builder.h"
#include "core/setup/fromCmdline.h"
#include "core/thread/MainThread.h"
#include "core/thread/ThreadedFactory.h"
#include "event/player.h"
#include "event/switch.h"
#include <QJsonDocument>
#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>

namespace features_adapter
{
namespace
{


event::player::Play::Playlist parsePlaylist(const cucumber::internal::Table& table)
{
  event::player::Play::Playlist result{};

  for (const auto& row : table.hashes()) {
    const auto name = row.at("name");
    const QUrl url = QString::fromStdString(name);
    result.push_back(url);
  }

  return result;
}

QString serializeJson(const QJsonDocument& value)
{
  const auto data = value.toJson();
  const auto serialized = QString{data};
  return serialized;
}

QString normalizeJson(const QString& value)
{
  QByteArray data{};
  data.append(value);
  const auto doc = QJsonDocument::fromJson(data);
  return serializeJson(doc);
}

QString normalizeJson(const QJsonObject& value)
{
  const auto doc = QJsonDocument{value};
  return serializeJson(doc);
}

QString serializeName(const QStringList& value)
{
  const auto serialized = value.join(".");
  return serialized;
}

void sendTo(const std::string& instance, const event::SharedEvent& event)
{
  const auto instanceName = QString::fromStdString(instance);

  cucumber::ScenarioScope<Context> context{};
  const auto component = context->app.findComponent(instanceName);
  if (!component) {
    throw std::runtime_error("instance not found: " + instance);
  }

  component->recvEvent(event);
}

const QString receiverName{"<receiver>"};


BEFORE()
{
  cucumber::ScenarioScope<Context> context;
  context->receiver.setObjectName(receiverName);
  context->app.connect(&context->receiver);
}

GIVEN("^I have the files:$")
{
  TABLE_PARAM(fileTable);

  cucumber::ScenarioScope<Context> context;
  for (const auto& row : fileTable.hashes()) {
    const auto filename = row.at("filename");
    context->fs.add(QString::fromStdString(filename), {});
  }
}

GIVEN("^I have the file \"([^\"]*)\":$")
{
  REGEX_PARAM(std::string, name);
  REGEX_PARAM(std::string, content);

  cucumber::ScenarioScope<Context> context;
  context->fs.add(QString::fromStdString(name), QString::fromStdString(content));
}

GIVEN("^I load the component \"([^\"]*)\" as \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, className);
  REGEX_PARAM(std::string, instanceName);

  cucumber::ScenarioScope<Context> context{};
  cucumber::ScenarioScope<Factories> factories{};

  core::setup::Builder builder{context->app, factories->repo, context->threadFactory};
  builder.buildComponent(QString::fromStdString(className), QString::fromStdString(instanceName), {});
  builder.buildConnection(QString::fromStdString(instanceName), receiverName);
}

WHEN("^I start the application with the arguments:$")
{
  REGEX_PARAM(std::string, rawArguments);
  const QStringList arguments = QString::fromStdString(rawArguments).split(QRegExp{"\\s"}, QString::SplitBehavior::SkipEmptyParts);

  cucumber::ScenarioScope<Context> context{};
  const auto args = core::setup::parseCmdline(arguments, context->fs);
  context->arguments = args;

  cucumber::ScenarioScope<Factories> factories{};
  core::setup::Builder builder{context->app, factories->repo, context->threadFactory};
  core::setup::fromArguments(args, builder, context->fs);
}

GIVEN("^I start the application as \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, name);

  cucumber::ScenarioScope<Context> context{};
  core::setup::Arguments args{};
  args.applicationName = QString::fromStdString(name);
  context->arguments = args;

  cucumber::ScenarioScope<Factories> factories{};
  core::setup::Builder builder{context->app, factories->repo, context->threadFactory};
  core::setup::fromArguments(args, builder, context->fs);
}

GIVEN("^I start the application boot process$")
{
  cucumber::ScenarioScope<Context> context;

  context->appStageChangeCompleted = false;
  context->app.boot();
}

GIVEN("^I boot the application$")
{
  cucumber::ScenarioScope<Context> context;

  context->appStageChangeCompleted = false;
  context->app.boot();

  while (!context->appStageChangeCompleted) {
    context->qapp.processEvents();
  }
}

WHEN("^I wait until the application boot process is finished$")
{
  cucumber::ScenarioScope<Context> context;

  while (!context->appStageChangeCompleted) {
    context->qapp.processEvents();
  }
}

WHEN("^I shutdown the application$")
{
  cucumber::ScenarioScope<Context> context;

  context->appStageChangeCompleted = false;
  context->app.shutdown();

  while (!context->appStageChangeCompleted) {
    context->qapp.processEvents();
  }
}

GIVEN("^I clear the content of \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, name);
  cucumber::ScenarioScope<Context> context;
  context->socketFactory.clear(QString::fromStdString(name));
}

WHEN("^I send to \"([^\"]*)\" the event \"switch\\.on\" with the id \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, instance);
  REGEX_PARAM(std::string, id);
  sendTo(instance, new event::switch_::On(QString::fromStdString(id)));
}

WHEN("^I send to \"([^\"]*)\" the event \"switch\\.off\" with the id \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, instance);
  REGEX_PARAM(std::string, id);
  sendTo(instance, new event::switch_::Off(QString::fromStdString(id)));
}

WHEN("^I send to \"([^\"]*)\" the event \"player\\.play\" with the playlist:$")
{
  REGEX_PARAM(std::string, instance);
  TABLE_PARAM(table);
  const auto playlist = parsePlaylist(table);
  sendTo(instance, new event::player::Play(playlist));
}

WHEN("^I send to \"([^\"]*)\" the event \"player\\.pause\"$")
{
  REGEX_PARAM(std::string, instance);
  sendTo(instance, new event::player::Pause());
}

WHEN("^I send to \"([^\"]*)\" the event \"player\\.continue\"$")
{
  REGEX_PARAM(std::string, instance);
  sendTo(instance, new event::player::Continue());
}

WHEN("^I receive the following content on \"([^\"]*)\":$")
{
  REGEX_PARAM(std::string, name);
  REGEX_PARAM(std::string, content);

  cucumber::ScenarioScope<Context> context;
  context->socketFactory.write(QString::fromStdString(name), QString::fromStdString(content));
}

THEN("^I expect the following content on \"([^\"]*)\":$")
{
  REGEX_PARAM(std::string, name);
  REGEX_PARAM(std::string, content);

  cucumber::ScenarioScope<Context> context;
  const auto actual = context->socketFactory.read(QString::fromStdString(name)).toStdString();

  ASSERT_EQ(content, actual);
}

WHEN("^I clear the cout$")
{
  cucumber::ScenarioScope<Context> rfidReader;
  rfidReader->cout.str({});
}

THEN("^I expect to see the cout:$")
{
  REGEX_PARAM(std::string, expected);

  cucumber::ScenarioScope<Context> rfidReader;
  ASSERT_EQ(expected, rfidReader->cout.str());
}

WHEN("^I clear all received events$")
{
  cucumber::ScenarioScope<Context> context{};
  context->receiver.clearEvents();
}

THEN("^I expect from \"([^\"]*)\" the event \"([^\"]*)\" with the content:$")
{
  REGEX_PARAM(std::string, instanceName);
  REGEX_PARAM(std::string, expectedEventName);
  REGEX_PARAM(std::string, expectedContentRaw);

  cucumber::ScenarioScope<Context> context{};
  const auto event = context->receiver.pop(instanceName);

  const auto actualEventName = serializeName((*event).name());
  ASSERT_EQ(expectedEventName, actualEventName.toStdString());

  const auto actualContent = normalizeJson((*event).content());
  const auto expectedContent = normalizeJson(QString::fromStdString(expectedContentRaw));
  ASSERT_EQ(expectedContent.toStdString(), actualContent.toStdString());
}

THEN("^I expect from \"([^\"]*)\" the event \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, instanceName);
  REGEX_PARAM(std::string, eventName);

  cucumber::ScenarioScope<Context> context{};
  const auto event = context->receiver.pop(instanceName);
  const auto actual = serializeName((*event).name());
  ASSERT_EQ(eventName, actual.toStdString());
}

THEN("^I expect no events from \"([^\"]*)\"$")
{
  REGEX_PARAM(std::string, instanceName);

  cucumber::ScenarioScope<Context> context{};
  ASSERT_EQ(0, context->receiver.eventCount(instanceName));
}

THEN("^I expect the application to be shutdown$")
{
  cucumber::ScenarioScope<Context> context{};

  ASSERT_TRUE(context->isShutdown);
}


}
}
