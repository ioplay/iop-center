/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "EventRecorder.h"

namespace features_adapter
{


void EventRecorder::recvEvent(const event::SharedEvent& event)
{
  events[sender()->objectName().toStdString()].push(event);
}

event::SharedEvent EventRecorder::pop(const std::string& instanceName)
{
  const auto idx = events.find(instanceName);
  if ((idx == events.end()) || idx->second.empty()) {
    throw std::runtime_error("no events received from " + instanceName);
  }

  const auto event = idx->second.front();
  idx->second.pop();

  return event;
}

std::size_t EventRecorder::eventCount(const std::string& instanceName) const
{
  const auto idx = events.find(instanceName);
  return (idx == events.end()) ? 0 : idx->second.size();
}

void EventRecorder::clearEvents()
{
  events.clear();
}


}
