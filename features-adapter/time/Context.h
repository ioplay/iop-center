/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "core/util/simulated/SimulatedTimer.h"

namespace features_adapter::time
{


class Context
{
public:
  core::util::simulated::SimulatedTimer timer{};
};


}
