/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Context.h"

#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>

namespace features_adapter::time
{
namespace
{


WHEN("^I wait for (\\d+) milliseconds$")
{
  REGEX_PARAM(unsigned, delta);
  cucumber::ScenarioScope<Context> context;

  context->timer.forwardTimeBy(std::chrono::milliseconds(delta));
}


}
}
