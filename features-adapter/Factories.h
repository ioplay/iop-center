/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Context.h"
#include "time/Context.h"
#include "core/component/rfid/simulated/SimRegisterDevice.h"
#include "core/setup/Factories.h"
#include "core/setup/FactoryRepository.h"
#include "core/setup/registerFactories.h"
#include "core/base/Configuration.h"
#include <gtest/gtest.h>
#include <cucumber-cpp/autodetect.hpp>

namespace features_adapter
{


class Factories
{
public:
  Factories()
  {
    core::setup::registerFactories(repo, factories);
  }

  cucumber::ScenarioScope<Context> context{};
  cucumber::ScenarioScope<time::Context> time{};
  cucumber::ScenarioScope<core::component::rfid::simulated::SimRegisterDevice> mfrc522;

  core::base::Configuration configuration{context->fs, context->arguments.value().applicationName};
  core::setup::Factories factories{context->cout, *mfrc522, context->socketFactory, context->fs, time->timer, configuration};
  core::setup::FactoryRepository repo{};

};


}
