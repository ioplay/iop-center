/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "player.h"
#include <QJsonArray>

namespace event::player
{


Play::Play(const Playlist& playlist_) :
  playlist{playlist_}
{
}

void Play::accept(Handler& visitor) const
{
  visitor.handle(*this);
}

QStringList Play::name() const
{
  return {"player", "play"};
}

QJsonObject Play::content() const
{
  QJsonArray array{};
  for (const auto& item : playlist) {
    array.append(item.toString());
  }
  return {
    {"playlist", array},
  };
}

void Pause::accept(Handler& visitor) const
{
  visitor.handle(*this);
}

QStringList Pause::name() const
{
  return {"player", "pause"};
}

QJsonObject Pause::content() const
{
  return {};
}

void Continue::accept(Handler& visitor) const
{
  visitor.handle(*this);
}

QStringList Continue::name() const
{
  return {"player", "continue"};
}

QJsonObject Continue::content() const
{
  return {};
}


}
