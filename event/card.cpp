/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "card.h"


namespace event::card
{
namespace
{


char nibbleToHex(uint8_t value)
{
  if (value < 10) {
    return '0' + value;
  } else {
    return 'a' + value - 10;
  }
}

QString byteToHex(uint8_t value)
{
  char buffer[3] = {0, 0, 0};
  buffer[0] = nibbleToHex(value >> 4);
  buffer[1] = nibbleToHex(value & 0x0f);
  buffer[2] = '\0';
  return QString{buffer};
}


}


Card::Card(const Id& id_) :
  id{id_}
{
}

QString Card::binaryIdString() const
{
  QString result{};
  for (const auto itr : id) {
    result += byteToHex(itr);
  }
  return result;
}

QJsonObject Card::content() const
{
  return {
    {"id", binaryIdString()},
  };
}

QStringList Detected::name() const
{
  return {"card", "detected"};
}

QStringList Lost::name() const
{
  return {"card", "lost"};
}


}
