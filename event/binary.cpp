/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "binary.h"


namespace event::binary
{


void On::accept(Handler& visitor) const
{
  visitor.handle(*this);
}

void Off::accept(Handler& visitor) const
{
  visitor.handle(*this);
}


}
