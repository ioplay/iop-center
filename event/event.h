/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <QStringList>
#include <QJsonObject>
#include <memory>

namespace event
{

struct Event
{
  virtual ~Event() = default;

  virtual QStringList name() const = 0;
  virtual QJsonObject content() const = 0;

protected:
  Event() = default;
  Event(const Event&) = default; // make sure nobody (also not implicitly) copies the Event. You have to copy a derived type of Event.
};


class SharedEvent
{
public:
  SharedEvent()
  {
  }

  SharedEvent(Event* evt) :
    event{std::shared_ptr<Event>(evt)}
  {
  }

  const Event& operator*() const
  {
    return *event.get();
  }

private:
  const std::shared_ptr<Event> event{};
};


class Handler
{
public:
  virtual ~Handler() = default;

  virtual void handle(const SharedEvent&) = 0;
};


template<typename E, typename C>
bool dispatch(const event::Event& event, C& handler)
{
  const auto specific = dynamic_cast<const E*>(&event);

  if (specific) {
    specific->accept(handler);
  }

  return specific;
}


}
