/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "event/switch.h"
#include <gtest/gtest.h>


namespace event::switch_::test
{
namespace
{

QStringList sl(const QStringList& value)
{
  return value;
}


TEST(switch_Test, On_returns_correct_name)
{
  const On testee{{}};

  ASSERT_EQ(sl({"switch", "on"}), testee.name());
}

TEST(switch_Test, Off_returns_correct_name)
{
  const Off testee{{}};

  ASSERT_EQ(sl({"switch", "off"}), testee.name());
}

TEST(switch_Test, On_returns_correct_content)
{
  const QJsonObject expected{{"id", "test id"}};
  const On testee{"test id"};

  ASSERT_EQ(expected, testee.content());
}


}
}
