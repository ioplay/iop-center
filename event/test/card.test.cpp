/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "event/card.h"
#include <gtest/gtest.h>


namespace event::card::test
{
namespace
{

QStringList sl(const QStringList& value)
{
  return value;
}


TEST(switch_Test, Detected_returns_correct_name)
{
  const Detected testee{{}};

  ASSERT_EQ(sl({"card", "detected"}), testee.name());
}

TEST(switch_Test, Lost_returns_correct_name)
{
  const Lost testee{{}};

  ASSERT_EQ(sl({"card", "lost"}), testee.name());
}

TEST(switch_Test, Detected_returns_correct_content)
{
  const QJsonObject expected{{"id", "012345"}};
  const Detected testee{{0x01, 0x23, 0x45}};

  ASSERT_EQ(expected, testee.content());
}


}
}
