/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "event/player.h"
#include <QJsonArray>
#include <gtest/gtest.h>


namespace event::player::test
{
namespace
{

QStringList sl(const QStringList& value)
{
  return value;
}


TEST(switch_Test, Play_returns_correct_name)
{
  const Play testee{{}};

  ASSERT_EQ(sl({"player", "play"}), testee.name());
}

TEST(switch_Test, Play_returns_correct_content)
{
  const QJsonObject expected{
    {"playlist", QJsonArray{
        "file:///song.mp3",
        "file:///music/test.ogg"}
    }};
  const Play testee{{QUrl{"file:///song.mp3"}, QUrl{"file:///music/test.ogg"}}};

  ASSERT_EQ(expected, testee.content());
}

TEST(switch_Test, Pause_returns_correct_name)
{
  const Pause testee{};

  ASSERT_EQ(sl({"player", "pause"}), testee.name());
}

TEST(switch_Test, Pause_returns_correct_content)
{
  const QJsonObject expected{};
  const Pause testee{};

  ASSERT_EQ(expected, testee.content());
}

TEST(switch_Test, Continue_returns_correct_name)
{
  const Continue testee{};

  ASSERT_EQ(sl({"player", "continue"}), testee.name());
}

TEST(switch_Test, Continue_returns_correct_content)
{
  const QJsonObject expected{};
  const Continue testee{};

  ASSERT_EQ(expected, testee.content());
}


}
}
