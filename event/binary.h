/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <QString>

namespace event::binary
{

class Handler;


struct Binary
{
  virtual void accept(Handler&) const = 0;

  virtual QString binaryIdString() const = 0;
};

struct On:
    public virtual Binary
{
  void accept(Handler& visitor) const override;
};

struct Off:
    public virtual Binary
{
  void accept(Handler& visitor) const override;
};


class Handler
{
public:
  virtual void handle(const On&) = 0;
  virtual void handle(const Off&) = 0;
};


}
