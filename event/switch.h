/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "event.h"
#include "binary.h"
#include <string>

namespace event::switch_
{


using Id = QString;

struct Switch :
    public Event,
    public virtual binary::Binary
{
  Switch(const Id&);

  Id id;

  QString binaryIdString() const override;
  QJsonObject content() const override;
};


struct On final :
    public Switch,
    public binary::On
{
  using Switch::Switch;

  QStringList name() const override;
};

struct Off final :
    public Switch,
    public binary::Off
{
  using Switch::Switch;

  QStringList name() const override;
};


}
