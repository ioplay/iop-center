/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "event.h"
#include "binary.h"
#include <vector>
#include <cstdint>

namespace event::card
{


using Id = std::vector<uint8_t>;

struct Card :
    public Event,
    public virtual binary::Binary
{
  Card(const Id&);

  Id id;

  QString binaryIdString() const override;
  QJsonObject content() const override;
};


struct Detected final :
    public Card,
    public binary::On
{
  using Card::Card;

  QStringList name() const override;
};

struct Lost final :
    public Card,
    public binary::Off
{
  using Card::Card;

  QStringList name() const override;
};


}
