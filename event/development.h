/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "event.h"

namespace event::development
{


struct Debug final :
    public Event
{
  Debug(const QJsonObject&);

  const QJsonObject data;

  QStringList name() const override;
  QJsonObject content() const override;
};


}
