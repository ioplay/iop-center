/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "development.h"

namespace event::development
{


Debug::Debug(const QJsonObject& data_) :
  data{data_}
{
}

QStringList Debug::name() const
{
  return {"development", "debug"};
}

QJsonObject Debug::content() const
{
  return data;
}


}
