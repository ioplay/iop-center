/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "event.h"
#include <QUrl>
#include <QList>

namespace event::player
{

class Handler;


struct Player :
    public Event
{
  virtual void accept(Handler&) const = 0;
};

struct Play final :
    public Player
{
  using Playlist = QList<QUrl>;

  Play(const Playlist&);
  void accept(Handler& visitor) const override;

  Playlist playlist;

  QStringList name() const override;
  QJsonObject content() const override;
};

struct Pause final :
    public Player
{
  using Player::Player;
  void accept(Handler& visitor) const override;

  QStringList name() const override;
  QJsonObject content() const override;
};

struct Continue final :
    public Player
{
  using Player::Player;
  void accept(Handler& visitor) const override;

  QStringList name() const override;
  QJsonObject content() const override;
};


class Handler
{
public:
  virtual void handle(const Play&) = 0;
  virtual void handle(const Pause&) = 0;
  virtual void handle(const Continue&) = 0;
};


}
