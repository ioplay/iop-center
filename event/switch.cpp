/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "switch.h"

namespace event::switch_
{


Switch::Switch(const Id& id_) :
  id{id_}
{
}

QString Switch::binaryIdString() const
{
  return id;
}

QJsonObject Switch::content() const
{
  return {
    {"id", id},
  };
}

QStringList On::name() const
{
  return {"switch", "on"};
}

QStringList Off::name() const
{
  return {"switch", "off"};
}


}
