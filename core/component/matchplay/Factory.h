#pragma once

#include "core/base/ComponentFactory.h"
#include <ostream>

namespace core::base
{

class Configuration;

}

namespace core::util
{

class Filesystem;

}

namespace core::component::matchplay
{


class Factory :
    public base::ComponentFactory
{
public:
  Factory(
      std::ostream&,
      const base::Configuration&,
      util::Filesystem&
      );

  base::Component* produce() override;

private:
  std::ostream& cout;
  const base::Configuration& configuration;
  util::Filesystem& filesystem;

};


}
