/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Matching.h"
#include "core/base/Configuration.h"
#include <QDir>

namespace core::component::matchplay
{


Matching::Matching(
    const base::Configuration& configuration_,
    const base::Component& component_
    ) :
  configuration{configuration_},
  component{component_}
{
}

void Matching::initializeWithRootDirectory(const QUrl& value)
{
  rootDirectory = value;
  configuration.load("matching", component, std::bind(&Matching::addMatch, this, std::placeholders::_1, std::placeholders::_2));
}

std::optional<QUrl> Matching::find(const CardId& value) const
{
  const auto idx = map.find(value);
  if (idx == map.end()) {
    return {};
  } else {
    return {*idx};
  }
}

bool Matching::contains(const QUrl& url) const
{
  return map.values().contains(url);
}

void Matching::addMatch(const QString& key, const QString& value)
{
  const QUrl uri = makeAbsolute(value);
  map.insert(key, uri);
}

QUrl Matching::makeAbsolute(const QUrl& value) const
{
  if (!value.isRelative()) {
    return value;
  }

  QUrl result = rootDirectory;
  result.setPath(QDir::cleanPath(rootDirectory.path() + "/" + value.path()));
  return result;
}


}
