/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "MatchingHelp.h"
#include "Settings.h"
#include "Matching.h"
#include "SupportedFiles.h"
#include "core/base/Configuration.h"
#include "core/util/Filesystem.h"

namespace core::component::matchplay
{


MatchingHelp::MatchingHelp(
    std::ostream& cout_,
    const Settings& settings_,
    const Matching& matching_,
    const base::Configuration& configuration_,
    const base::Component& component_,
    const util::Filesystem& fs_
    ) :
  cout{cout_},
  settings{settings_},
  matching{matching_},
  configuration{configuration_},
  component{component_},
  fs{fs_}
{
}

void MatchingHelp::print()
{
  const auto root = settings.getRootDirectory();
  if (!root.isValid()) {
    return;
  }

  const auto path = root.path();

  QStringList directoriesWithSupportedFiles{};
  listDirectoriesWithSupportedFiles(path, directoriesWithSupportedFiles);

  const auto configDir = configuration.directory(component);
  cout << "possible entries for matching configuration in " << configDir.toStdString() << ":" << std::endl;
  for (const auto& dir: directoriesWithSupportedFiles) {
    cout << "  " << dir.toStdString() << std::endl;
  }
}

void MatchingHelp::listDirectoriesWithSupportedFiles(const QDir& path, QStringList& result) const
{
  const auto entries = fs.listDirectories(path);
  for (const auto& file : entries) {
    const QDir dirname{path.filePath(file)};
    const auto ignored = directoryIsIgnored(dirname);
    if (!ignored) {
      const auto isConfigured = directoryIsConfigured(dirname);
      if (!isConfigured) {
        const auto hasSupportedFile = directoryContainsSupportedFiles(dirname);
        if (hasSupportedFile) {
          const auto relative = relativePath(dirname);
          result.append(relative);
        }
      }
    }

    listDirectoriesWithSupportedFiles(dirname, result);
  }
}

bool MatchingHelp::directoryIsIgnored(const QDir& path)
{
  const QFileInfo filename{path.path()};
  return filename.isHidden();
}

bool MatchingHelp::directoryIsConfigured(const QDir& dirname) const
{
  const auto root = settings.getRootDirectory();
  QUrl absUrl = root;
  absUrl.setPath(dirname.path());
  return matching.contains(absUrl);
}

bool MatchingHelp::directoryContainsSupportedFiles(const QDir& dirname) const
{
  const auto entries = fs.listFiles(dirname);
  for (const auto& file : entries) {
    if (isSupportedFile(file)) {
      return true;
    }
  }

  return false;
}

QString MatchingHelp::relativePath(const QDir& dirname) const
{
  const auto root = settings.getRootDirectory();
  const QDir rootPath = root.path();
  const auto relative = rootPath.relativeFilePath(dirname.path()) + "/";
  return relative;
}


}
