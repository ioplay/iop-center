/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "SupportedFiles.h"
#include <QFileInfo>
#include <QSet>

namespace core::component::matchplay
{
namespace
{


//TODO get the following list automatically from mpd
//TODO read the following list from a file
//TODO update the following list from time to time. call on the target: echo decoders | nc localhost 6600 | grep suffix | sort | uniq
const QSet<QString> SupportedSuffixes
{
  "669",
  "amf",
  "ams",
  "dbm",
  "dff",
  "dfm",
  "dsf",
  "dsm",
  "far",
  "flac",
  "it",
  "mdl",
  "med",
  "mod",
  "mp3",
  "mt2",
  "mtm",
  "oga",
  "ogg",
  "okt",
  "s3m",
  "stm",
  "ult",
  "umx",
  "xm",
};


}


bool isSupportedFile(const QString& filename)
{
  const QFileInfo info{filename};
  const auto suffix = info.suffix();
  const auto isSupported = SupportedSuffixes.contains(suffix);
  return isSupported;
}


}
