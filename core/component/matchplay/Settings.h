/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <QMap>
#include <QUrl>
#include <optional>

namespace core::base
{

class Configuration;
class Component;

}

namespace core::component::matchplay
{


class Settings
{
public:
  Settings(
      const base::Configuration&,
      const base::Component&
      );

  void initialize();

  QUrl getRootDirectory() const;
  QUrl getStartedFile() const;

private:
  const base::Configuration& configuration;
  const base::Component& component;
  QUrl rootDirectory{};
  QUrl startedFile{};

  void handleSetting(const QString& key, const QString& value);
};


}
