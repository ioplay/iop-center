/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <QString>

namespace core::component::matchplay
{


bool isSupportedFile(const QString& filename);


}
