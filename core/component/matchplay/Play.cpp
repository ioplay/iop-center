/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Play.h"
#include "PlaylistGenerator.h"
#include "Settings.h"
#include "Matching.h"
#include "event/player.h"
#include "core/util/Filesystem.h"
#include <QDebug>

namespace core::component::matchplay
{


Play::Play(
    const Settings& settings_,
    const Matching& matching_,
    const PlaylistGenerator& playlistGenerator_,
    event::Handler& handler_,
    const util::Filesystem& fs_
    ) :
  settings{settings_},
  matching{matching_},
  playlistGenerator{playlistGenerator_},
  handler{handler_},
  fs{fs_}
{
}

void Play::started()
{
  playStarted();
}

void Play::handle(const event::binary::On& event)
{
  const auto id = event.binaryIdString();

  if (id == lastCard) {
    handler.handle(new event::player::Continue());
  } else {
    const auto uri = matching.find(id);
    if (uri) {
      lastCard = id;
      const auto playlist = playlistGenerator.createPlaylist(*uri);
      handler.handle(new event::player::Play(playlist));
    } else {
      qInfo() << "no action for:" << id;
    }
  }
}

void Play::handle(const event::binary::Off& event)
{
  const auto id = event.binaryIdString();

  if (id == lastCard) {
    handler.handle(new event::player::Pause());
  }
}

void Play::playStarted()
{
  const auto uri = settings.getStartedFile();
  if (!uri.isValid()) {
    return;
  }

  const auto path = uri.path();
  const auto doesExist = fs.exists(path);
  const auto filename = uri.toString();
  if (!doesExist) {
    qDebug() << "file not found:" << filename;
    return;
  }

  handler.handle(new event::player::Play({filename}));
}


}
