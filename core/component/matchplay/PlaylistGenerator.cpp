/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "PlaylistGenerator.h"
#include "SupportedFiles.h"
#include "core/util/Filesystem.h"

namespace core::component::matchplay
{


PlaylistGenerator::PlaylistGenerator(
    const util::Filesystem& fs_
    ) :
  fs{fs_}
{
}

PlaylistGenerator::Playlist PlaylistGenerator::createPlaylist(const QUrl& uri) const
{
  if (!uri.isLocalFile()) {
    return Playlist{uri};
  }

  const auto path = uri.path();
  const auto doesExist = fs.exists(path);
  if (!doesExist) {
    return {};
  }
  const auto isDirectory = fs.isDirectory(path);
  const auto playlist = isDirectory ? listFiles(path) : Playlist{uri};
  return playlist;
}

PlaylistGenerator::Playlist PlaylistGenerator::listFiles(const QDir& directory) const
{
  Playlist result{};

  const auto entries = fs.listFiles(directory);
  for (const auto& file : entries) {
    const auto isSupported = isSupportedFile(file);
    if (isSupported) {
      const auto path = directory.filePath(file);

      QUrl url;
      url.setScheme("file");
      url.setPath(path);

      result.push_back(url);
    }
  }

  std::sort(result.begin(), result.end());

  return result;
}


}
