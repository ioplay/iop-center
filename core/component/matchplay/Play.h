/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "event/binary.h"
#include "event/event.h"

namespace core::util
{

class Filesystem;

}

namespace core::component::matchplay
{

class Settings;
class Matching;
class PlaylistGenerator;


class Play :
    public event::binary::Handler
{
public:
  Play(
      const Settings&,
      const Matching&,
      const PlaylistGenerator&,
      event::Handler&,
      const util::Filesystem&
      );

  void started();

  void handle(const event::binary::On&) override;
  void handle(const event::binary::Off&) override;

private:
  const Settings& settings;
  const Matching& matching;
  const PlaylistGenerator& playlistGenerator;
  event::Handler& handler;
  const util::Filesystem& fs;

  QString lastCard{};

  void playStarted();
};


}
