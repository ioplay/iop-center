/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Settings.h"
#include "core/base/Configuration.h"

namespace core::component::matchplay
{


Settings::Settings(
    const base::Configuration& configuration_,
    const base::Component& component_
    ) :
  configuration{configuration_},
  component{component_}
{
}

void Settings::initialize()
{
  configuration.load("settings", component, std::bind(&Settings::handleSetting, this, std::placeholders::_1, std::placeholders::_2));
}

QUrl Settings::getRootDirectory() const
{
  return rootDirectory;
}

QUrl Settings::getStartedFile() const
{
  return startedFile;
}

void Settings::handleSetting(const QString& key, const QString& value)
{
  if (key == "root-directory") {
    rootDirectory = value;
  } else if (key == "started-file") {
    startedFile = value;
  }
}


}
