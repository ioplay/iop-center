/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <ostream>
#include <QDir>

namespace core::base
{

class Configuration;
class Component;

}

namespace core::util
{

class Filesystem;

}

namespace core::component::matchplay
{

class Settings;
class Matching;


class MatchingHelp
{
public:
  MatchingHelp(
      std::ostream&,
      const Settings&,
      const Matching&,
      const base::Configuration&,
      const base::Component&,
      const util::Filesystem&
      );

  void print();

private:
  std::ostream& cout;
  const Settings& settings;
  const Matching& matching;
  const base::Configuration& configuration;
  const base::Component& component;
  const util::Filesystem& fs;

  void listDirectoriesWithSupportedFiles(const QDir&, QStringList&) const;
  static bool directoryIsIgnored(const QDir&) ;
  bool directoryIsConfigured(const QDir&) const;
  bool directoryContainsSupportedFiles(const QDir&) const;
  QString relativePath(const QDir&) const;
};


}
