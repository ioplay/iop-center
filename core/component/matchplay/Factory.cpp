#include "Factory.h"
#include "Component.h"

namespace core::component::matchplay
{


Factory::Factory(
    std::ostream& cout_,
    const base::Configuration& configuration_,
    util::Filesystem& filesystem_
    ) :
  cout{cout_},
  configuration{configuration_},
  filesystem{filesystem_}
{
}

base::Component* Factory::produce()
{
  return new Component(cout, configuration, filesystem);
}


}
