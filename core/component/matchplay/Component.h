/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Play.h"
#include "Settings.h"
#include "Matching.h"
#include "PlaylistGenerator.h"
#include "MatchingHelp.h"
#include "core/base/Component.h"
#include <ostream>

namespace core::component::matchplay
{


class Component :
    public base::Component
{
public:
  Component(
      std::ostream&,
      const base::Configuration&,
      util::Filesystem&
      );

  void initialize() override;
  void start() override;

  void recvEvent(const event::SharedEvent&) override;

private:
  base::EventAdapter eventAdapter;
  Settings settings;
  Matching matching;
  PlaylistGenerator playlistGenerator;
  MatchingHelp matchingHelp;
  Play player;

};


}
