/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Component.h"

namespace core::component::matchplay
{


Component::Component(
    std::ostream& cout,
    const base::Configuration& configuration,
    util::Filesystem& filesystem
    ) :
  eventAdapter{*this},
  settings{configuration, *this},
  matching{configuration, *this},
  playlistGenerator{filesystem},
  matchingHelp{cout, settings, matching, configuration, *this, filesystem},
  player{settings, matching, playlistGenerator, eventAdapter, filesystem}
{
}

void Component::initialize()
{
  settings.initialize();
  matching.initializeWithRootDirectory(settings.getRootDirectory());
  initializeComplete();
}

void Component::start()
{
  player.started();
  matchingHelp.print();
  startComplete();
}

void Component::recvEvent(const event::SharedEvent& event)
{
  event::dispatch<event::binary::Binary, event::binary::Handler>(*event, player);
}


}
