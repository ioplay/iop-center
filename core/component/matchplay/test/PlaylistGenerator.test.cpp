/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "../PlaylistGenerator.h"
#include "core/util/simulated/Filesystem.h"
#include <gtest/gtest.h>


namespace core::component::matchplay::test
{
namespace
{


class matchplay_PlaylistGenerator_Test :
    public testing::Test
{
public:
  util::simulated::Filesystem filesystem{};
  PlaylistGenerator testee{filesystem};

  static PlaylistGenerator::Playlist p(const PlaylistGenerator::Playlist& value)
  {
    return value;
  }

};


TEST_F(matchplay_PlaylistGenerator_Test, returns_empty_playlist_when_file_does_not_exist)
{

  const auto playlist = testee.createPlaylist({"file:///music/song.mp3"});

  ASSERT_EQ(p({}), playlist);
}

TEST_F(matchplay_PlaylistGenerator_Test, returns_file_in_playlist_when_file_does_exist)
{
  filesystem.add("/music/song.mp3", {});

  const auto playlist = testee.createPlaylist({"file:///music/song.mp3"});

  ASSERT_EQ(p({{"file:///music/song.mp3"}}), playlist);
}

TEST_F(matchplay_PlaylistGenerator_Test, returns_empty_playlist_when_directory_does_not_exist)
{

  const auto playlist = testee.createPlaylist({"file:///music/"});

  ASSERT_EQ(p({}), playlist);
}

TEST_F(matchplay_PlaylistGenerator_Test, returns_files_in_playlist_when_directory_does_exist)
{
  filesystem.add("/music/song1.mp3", {});
  filesystem.add("/music/song2.mp3", {});
  filesystem.add("/music/song3.mp3", {});

  const auto playlist = testee.createPlaylist({"file:///music/"});

  ASSERT_EQ(p({{"file:///music/song1.mp3"}, {"file:///music/song2.mp3"}, {"file:///music/song3.mp3"}}), playlist);
}

TEST_F(matchplay_PlaylistGenerator_Test, returns_only_supported_files_in_playlist_when_directory_is_provided)
{
  filesystem.add("/music/song1.mp3", {});
  filesystem.add("/music/song2.txt", {});
  filesystem.add("/music/song3.mp3", {});

  const auto playlist = testee.createPlaylist({"file:///music/"});

  ASSERT_EQ(p({{"file:///music/song1.mp3"}, {"file:///music/song3.mp3"}}), playlist);
}

TEST_F(matchplay_PlaylistGenerator_Test, does_not_return_files_in_subdirectories_when_directory_is_provided)
{
  filesystem.add("/music/artist1/song.ogg", {});
  filesystem.add("/music/song.ogg", {});
  filesystem.add("/music/artist3/song.ogg", {});

  const auto playlist = testee.createPlaylist({"file:///music/"});

  ASSERT_EQ(p({{"file:///music/song.ogg"}}), playlist);
}

TEST_F(matchplay_PlaylistGenerator_Test, return_https_entry_as_playlist)
{

  const auto playlist = testee.createPlaylist({"https://stream.radioparadise.com/rp_192m.ogg"});

  ASSERT_EQ(p({{"https://stream.radioparadise.com/rp_192m.ogg"}}), playlist);
}

TEST_F(matchplay_PlaylistGenerator_Test, return_http_entry_as_playlist)
{

  const auto playlist = testee.createPlaylist({"http://stream.radio.com/test.mp3"});

  ASSERT_EQ(p({{"http://stream.radio.com/test.mp3"}}), playlist);
}


}
}
