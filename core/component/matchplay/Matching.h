/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <QMap>
#include <QUrl>
#include <optional>

namespace core::base
{

class Configuration;
class Component;

}

namespace core::component::matchplay
{


class Matching
{
public:
  using CardId = QString;
  using Map = QMap<CardId, QUrl>;

  Matching(
      const base::Configuration&,
      const base::Component&
      );

  void initializeWithRootDirectory(const QUrl&);

  std::optional<QUrl> find(const CardId&) const;
  bool contains(const QUrl&) const;

private:
  const base::Configuration& configuration;
  const base::Component& component;
  Map map{};
  QUrl rootDirectory{};

  void addMatch(const QString& key, const QString& value);
  QUrl makeAbsolute(const QUrl&) const;
};


}
