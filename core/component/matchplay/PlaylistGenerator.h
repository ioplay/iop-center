/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <QUrl>
#include <QList>
#include <QDir>

namespace core::util
{

class Filesystem;

}

namespace core::component::matchplay
{


class PlaylistGenerator
{
public:
  using Playlist = QList<QUrl>;

  PlaylistGenerator(
      const util::Filesystem&
      );

  Playlist createPlaylist(const QUrl&) const;

private:
  const util::Filesystem& fs;

  Playlist listFiles(const QDir&) const;

};


}
