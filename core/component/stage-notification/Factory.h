/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "core/base/ComponentFactory.h"

namespace core::component::stage_notification
{


class Factory :
    public base::ComponentFactory
{
public:
  base::Component* produce() override;

};


}
