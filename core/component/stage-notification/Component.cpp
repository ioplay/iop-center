/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Component.h"
#include "event/development.h"

namespace core::component::stage_notification
{


void Component::initialize()
{
  sendStageChange("initialize");
  base::Component::initialize();
}

void Component::start()
{
  sendStageChange("start");
  base::Component::start();
}

void Component::stop()
{
  sendStageChange("stop");
  base::Component::stop();
}

void Component::deinitialize()
{
  sendStageChange("deinitialize");
  base::Component::deinitialize();
}

void Component::sendStageChange(const QString& stage)
{
  sendDebug({
    {"stage-change", stage},
  });
}

void Component::sendDebug(const QJsonObject& data)
{
  sendEvent(new event::development::Debug(data));
}


}
