/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Factory.h"
#include "Component.h"

namespace core::component::stage_notification
{


base::Component* Factory::produce()
{
  return new Component();
}


}
