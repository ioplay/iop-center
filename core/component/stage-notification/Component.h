/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "core/base/Component.h"

namespace core::component::stage_notification
{


class Component :
    public base::Component
{
public:
  void initialize() override;
  void start() override;
  void stop() override;
  void deinitialize() override;

private:
  void sendStageChange(const QString&);
  void sendDebug(const QJsonObject&);
};


}
