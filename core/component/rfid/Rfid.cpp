#include "Rfid.h"
#include "RegisterDevice.h"

namespace core::component::rfid
{


Rfid::Rfid(event::Handler &observer, RegisterDevice& regdev_, util::Timer &timer_) :
  regdev{regdev_},
  mfrc522{regdev, timer_},
  card{mfrc522},
  reader{card, observer},
  timer{timer_}
{
}

void Rfid::initialize(const Success& success, const Error& error)
{
  regdev.open();
  if (regdev.isOpen()) {
    mfrc522.init();
    card.init();
    reader.init();
    success();
  } else {
    error();
  }
}

void Rfid::start()
{
  timerId = timer.startCyclic(std::chrono::milliseconds(50), std::bind(&Rfid::timeout, this, std::placeholders::_1));
}

void Rfid::stop()
{
  timer.stop(timerId);
}

void Rfid::deinitialize()
{
}

void Rfid::timeout(util::Timer::Id)
{
  reader.cycle();
}


}
