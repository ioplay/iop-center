#include "Component.h"

namespace core::component::rfid
{


Component::Component(RegisterDevice& mfrc522Registers, util::Timer& timer) :
  eventAdapter{*this},
  rfid{eventAdapter, mfrc522Registers, timer}
{
}

void Component::initialize()
{
  rfid.initialize(
        std::bind(&Component::initializeComplete, this),
        std::bind(&Component::initializeError, this));
}

void Component::start()
{
  rfid.start();
  startComplete();
}

void Component::stop()
{
  rfid.stop();
  stopComplete();
}

void Component::deinitialize()
{
  rfid.deinitialize();
  deinitializeComplete();
}


}
