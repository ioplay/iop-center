#pragma once

#include "Rfid.h"
#include "core/base/Component.h"
#include "core/util/Timer.h"
#include "event/event.h"


namespace core::component::rfid
{


class Component :
    public core::base::Component
{
public:
  Component(RegisterDevice&, util::Timer&);

  void initialize() override;
  void start() override;
  void stop() override;
  void deinitialize() override;

private:
  base::EventAdapter eventAdapter;
  Rfid rfid;

};


}

