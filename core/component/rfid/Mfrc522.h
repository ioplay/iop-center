#pragma once

#include <vector>
#include <optional>
#include <functional>

namespace core::util
{

class Timer;

}

namespace core::component::rfid
{

class RegisterDevice;


class Mfrc522
{
public:
  Mfrc522(RegisterDevice&, util::Timer&);

  struct CardTransfer
  {
    std::vector<uint8_t> data;
    std::size_t numberOfBits;
  };

  void init();

  using Success = std::function<void(const CardTransfer&)>;
  using Error = std::function<void()>;
  void transfer(const CardTransfer&, const Success&, const Error&);

private:
  RegisterDevice& device;
  util::Timer& timer;

  enum class State
  {
    Uninitialized,
    Idle,
    Transmitting,
  };
  State state{State::Uninitialized};
  Success success;
  Error error;

  void transmitTimeout();
  static bool hasDataReceived(uint8_t irq, uint8_t error) ;

};


}
