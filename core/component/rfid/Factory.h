#pragma once

#include "core/base/ComponentFactory.h"

namespace core::util
{

class Timer;

}

namespace core::component::rfid
{

class RegisterDevice;


class Factory :
    public base::ComponentFactory
{
public:
  Factory(RegisterDevice&, util::Timer&);

  base::Component* produce() override;

private:
  RegisterDevice& mfrc522Registers;
  util::Timer& timer;

};


}
