#include "Factory.h"
#include "Component.h"
#include "RegisterDevice.h"

namespace core::component::rfid
{


Factory::Factory(RegisterDevice& mfrc522Registers_, util::Timer& timer_) :
  mfrc522Registers{mfrc522Registers_},
  timer{timer_}
{
}

base::Component* Factory::produce()
{
  return new Component(mfrc522Registers, timer);
}


}
