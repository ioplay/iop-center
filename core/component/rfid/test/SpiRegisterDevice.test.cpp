/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "core/component/rfid/SpiRegisterDevice.h"
#include "core/component/rfid/simulated/SimulatedSpi.h"
#include <gtest/gtest.h>


namespace core::component::rfid::test
{
namespace
{


class rfid_SpiRegisterDevice_Test :
    public testing::Test
{
public:
  simulated::SimulatedSpi spi{};
  SpiRegisterDevice testee{spi};

  static std::vector<Spi::Buffer> b(const std::vector<Spi::Buffer>& value)
  {
    return value;
  }

  static RegisterDevice::RegisterSequence r(const RegisterDevice::RegisterSequence& value)
  {
    return value;
  }

};


TEST_F(rfid_SpiRegisterDevice_Test, convert_and_transfer_data_when_writing)
{
  spi.rc.push({});

  testee.write({{0x01, 0x04}, {0x02, 0x05}, {0x03, 0x06}});

  ASSERT_EQ(1, spi.tx.size());
  ASSERT_EQ(b({{0x02, 0x04}, {0x04, 0x05}, {0x06, 0x06}}), spi.tx.front());
}

TEST_F(rfid_SpiRegisterDevice_Test, convert_and_transfer_data_when_reading)
{
  spi.rc.push({{{}, 0x04}, {{}, 0x05}, {{}, 0x06}});

  const auto result = testee.read({0x01, 0x02, 0x03});

  ASSERT_EQ(1, spi.tx.size());
  ASSERT_EQ(b({{0x82, 0x00}, {0x84, 0x00}, {0x86, 0x00}}), spi.tx.front());
  ASSERT_EQ(0, spi.rc.size());
  ASSERT_EQ(r({{0x01, 0x04}, {0x02, 0x05}, {0x03, 0x06}}), result);
}


}
}
