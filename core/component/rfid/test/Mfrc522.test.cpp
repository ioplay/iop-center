/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "core/component/rfid/Mfrc522.h"
#include "core/component/rfid/simulated/SimRegisterDevice.h"
#include "core/util/simulated/SimulatedTimer.h"
#include <gtest/gtest.h>


namespace core::component::rfid::test
{
namespace
{


class rfid_mfrc522_Test :
    public testing::Test
{
public:
  simulated::SimRegisterDevice registers{};
  util::simulated::SimulatedTimer timer{};
  Mfrc522 testee{registers, timer};

};


TEST_F(rfid_mfrc522_Test, returns_data_from_device_when_reading)
{
  Mfrc522::CardTransfer recv{};
  registers.txRegisters[0x04] = {0x64};
  registers.txRegisters[0x06] = {0x00};
  registers.txRegisters[0x0A] = {0x03};
  registers.txRegisters[0x0C] = {0x10};
  registers.txRegisters[0x09] = {0x42, 0x23, 0x57};
  testee.init();

  testee.transfer({}, [&recv](const Mfrc522::CardTransfer& value){
    recv = value;
  }, []{});
  timer.forwardTimeBy(std::chrono::milliseconds(5));

  ASSERT_EQ(24, recv.numberOfBits);
  ASSERT_EQ(0x42, recv.data.at(0));
  ASSERT_EQ(0x23, recv.data.at(1));
  ASSERT_EQ(0x57, recv.data.at(2));
}

TEST_F(rfid_mfrc522_Test, returns_error_when_reading_failed)
{
  Mfrc522::CardTransfer recv{};
  bool readSuccess{false};
  bool readError{false};
  registers.canRead = false;
  testee.init();

  testee.transfer({}, [&readSuccess](const Mfrc522::CardTransfer&){
    readSuccess = true;
  }, [&readError]{
    readError = true;
  });
  timer.forwardTimeBy(std::chrono::milliseconds(5));

  EXPECT_FALSE(readSuccess);
  ASSERT_TRUE(readError);
}


}
}
