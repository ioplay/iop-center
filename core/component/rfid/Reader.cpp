#include "Reader.h"
#include "event/card.h"
#include "event/event.h"

namespace core::component::rfid
{


Reader::Reader(Card& card_, event::Handler& observer_) :
  device{card_},
  observer{observer_}
{
}

void Reader::init()
{
  state = State::NoCard_Idle;
}

void Reader::cycle()
{
  switch (state) {
    case State::NoCard_Idle: {
      state = State::NoCard_WaitType;
      device.requestType(std::bind(&Reader::typeSuccess, this, std::placeholders::_1), std::bind(&Reader::error, this));
      break;
    }

    case State::HasCard_Idle:
      state = State::HasCard_WaitId;
      device.requestId(std::bind(&Reader::idSuccess, this, std::placeholders::_1), std::bind(&Reader::error, this));
      break;

    case State::Uninitialized:
    case State::NoCard_WaitType:
    case State::NoCard_WaitId:
    case State::HasCard_WaitId:
      break;
  }
}

void Reader::typeSuccess(uint8_t)
{
  switch (state) {
    case State::NoCard_WaitType:
      state = State::NoCard_WaitId;
      device.requestId(std::bind(&Reader::idSuccess, this, std::placeholders::_1), std::bind(&Reader::error, this));
      break;

    case State::Uninitialized:
    case State::NoCard_Idle:
    case State::NoCard_WaitId:
    case State::HasCard_Idle:
    case State::HasCard_WaitId:
      break;
  }
}

void Reader::idSuccess(const std::vector<uint8_t>& id)
{
  switch (state) {
    case State::NoCard_WaitId:
      state = State::HasCard_Idle;
      cardId = id;
      observer.handle(new event::card::Detected(id));
      break;

    case State::HasCard_WaitId:
      state = State::HasCard_Idle;
      break;

    case State::Uninitialized:
    case State::NoCard_Idle:
    case State::NoCard_WaitType:
    case State::HasCard_Idle:
      break;
  }
}

void Reader::error()
{
  switch (state) {
    case State::NoCard_Idle:
    case State::NoCard_WaitType:
    case State::NoCard_WaitId:
      state = State::NoCard_Idle;
      break;

    case State::HasCard_WaitId:
      state = State::NoCard_Idle;
      observer.handle(new event::card::Lost(cardId));
      cardId = {};
      break;

    case State::Uninitialized:
    case State::HasCard_Idle:
      break;
  }
}


}
