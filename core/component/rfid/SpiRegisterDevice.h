#pragma once

#include "core/component/rfid/RegisterDevice.h"

namespace core::component::rfid
{

class Spi;


class SpiRegisterDevice :
    public RegisterDevice
{
public:
  SpiRegisterDevice(Spi&);

  bool isOpen() const override;
  void open() override;
  void write(const RegisterSequence&) override;
  RegisterSequence read(const ReadRequestSequence&) override;

private:
  Spi& spi;
};



}
