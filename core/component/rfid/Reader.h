#pragma once

#include "Card.h"

namespace event
{

class Handler;

}

namespace core::component::rfid
{


class Reader
{
public:
  using CardId = std::vector<uint8_t>;

  Reader(Card&, event::Handler&);

  void init();
  void cycle();

private:
  Card& device;
  event::Handler& observer;
  enum class State
  {
    Uninitialized,
    NoCard_Idle,
    NoCard_WaitType,
    NoCard_WaitId,
    HasCard_Idle,
    HasCard_WaitId,
  };
  State state{State::Uninitialized};

  CardId cardId{};

  void typeSuccess(uint8_t);
  void idSuccess(const std::vector<uint8_t>&);
  void error();
};


}
