#include "SpiRegisterDevice.h"
#include "Spi.h"
#include <cassert>

namespace core::component::rfid
{


SpiRegisterDevice::SpiRegisterDevice(Spi& spi_) :
  spi{spi_}
{
}

bool SpiRegisterDevice::isOpen() const
{
  return spi.isOpen();
}

void SpiRegisterDevice::open()
{
  spi.open(0, 0);
  spi.setMaxSpeedHz(1000000);
}

void SpiRegisterDevice::write(const RegisterDevice::RegisterSequence& sequence)
{
  std::vector<Spi::Buffer> buffers{};

  for (const auto& reg : sequence) {
    Spi::Buffer buffer{uint8_t((reg.address << 1) & 0x7E)};
    buffer.insert(buffer.end(), reg.value.begin(), reg.value.end());
    buffers.push_back(buffer);
  }

  spi.transfer(buffers);
}

RegisterDevice::RegisterSequence SpiRegisterDevice::read(const ReadRequestSequence& sequence)
{
  std::vector<Spi::Buffer> buffers{};
  for (const auto& itr : sequence) {

    Spi::Buffer buffer(itr.count+1, uint8_t(((itr.address << 1) & 0x7E) | 0x80));
    buffer[itr.count] = 0;
    buffers.push_back(buffer);
  }

  const auto recv = spi.transfer(buffers);

  RegisterSequence result{};
  for (std::size_t i = 0; i < recv.size(); i++) {
    const auto rx = recv.at(i);
    const auto& tx = sequence.at(i);
    const std::vector<uint8_t> rxbuf{rx.begin()+1, rx.end()};
    result.emplace_back(tx.address, rxbuf);
  }

  return result;
}


}
