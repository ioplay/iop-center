#pragma once

#include "../Spi.h"
#include "core/util/Logger.h"
#include <cstdint>
#include <vector>

namespace core::component::rfid::adapter
{


class SpiDev :
    public Spi
{
public:
  void open(int bus, int device) override;
  bool isOpen() const override;

  void setMaxSpeedHz(uint32_t value) override;
  std::vector<Buffer> transfer(const std::vector<Buffer>&) override;

private:
  int fd{-1};
  uint32_t max_speed_hz = 0;
  util::Logger logger{"spi"};

};


}
