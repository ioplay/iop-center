#include "SpiDev.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <fcntl.h>
#include <linux/ioctl.h>
#include <linux/spi/spidev.h>
#include <linux/types.h>
#include <memory.h>
#include <stdexcept>
#include <sys/ioctl.h>
#include <unistd.h>

namespace core::component::rfid::adapter
{


void SpiDev::open(int bus, int device)
{
  const auto path = std::string("/dev/spidev") + std::to_string(bus) + std::string(".") + std::to_string(device);
  uint32_t tmp32;
  fd = ::open(path.c_str(), O_RDWR, 0);
  if (fd < 0) {
    perror("open spi device");
  } else {
    if (ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &tmp32) == -1) {
      perror("ioctl SPI_IOC_RD_MAX_SPEED_HZ");
    } else {
      max_speed_hz = tmp32;
    }
  }
}

bool SpiDev::isOpen() const
{
  return fd >= 0;
}

void SpiDev::setMaxSpeedHz(uint32_t value)
{
  max_speed_hz = value;
}

std::vector<SpiDev::Buffer> SpiDev::transfer(const std::vector<Buffer> &txbuf)
{
  std::vector<struct spi_ioc_transfer> xfer{};
  std::vector<Buffer> result{};

  for (const auto& buffer : txbuf) {
    {
      result.emplace_back(buffer.size(), 0);

      struct spi_ioc_transfer xp{};

      xp.tx_buf = (unsigned long)buffer.data();
      xp.rx_buf = (unsigned long)result.back().data();
      xp.len = buffer.size();
      xp.delay_usecs = 0;
      xp.speed_hz = max_speed_hz;
      xp.bits_per_word = 8;
      xp.cs_change = 1;

      xfer.push_back(xp);
    }

    {
      struct spi_ioc_transfer xp{};

      xp.len = 0;
      xp.cs_change = 1;

      xfer.push_back(xp);
    }
  }

  std::size_t len = 0;
  for (const auto& xp : xfer) {
    len += xp.len;
  }

  const auto status = ioctl(fd, SPI_IOC_MESSAGE(xfer.size()), xfer.data());
  if (status < 0) {
    perror("ioctl SPI_IOC_MESSAGE");
    return {};
  }

  if (status != len) {
    throw std::runtime_error("wrong size " + std::to_string(status));
  }

  return result;
}


}
