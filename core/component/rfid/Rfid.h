#pragma once

#include "Reader.h"
#include "Mfrc522.h"
#include "Card.h"
#include "core/util/Timer.h"

namespace core::component::rfid
{

class RegisterDevice;


class Rfid
{
public:
  Rfid(event::Handler&, RegisterDevice&, util::Timer&);

  using Success = std::function<void()>;
  using Error = std::function<void()>;
  void initialize(const Success&, const Error&);
  void start();

  void stop();
  void deinitialize();

private:
  RegisterDevice& regdev;
  Mfrc522 mfrc522;
  Card card;
  Reader reader;
  util::Timer& timer;

  util::Timer::Id timerId{};

  void timeout(util::Timer::Id);
};


}
