#include "Card.h"
#include <iostream>

namespace core::component::rfid
{


Card::Card(Mfrc522& rfid_) :
  rfid{rfid_}
{
}

void Card::init()
{
  state = State::Idle;
}

void Card::requestType(const SuccessType& success, const Error& error)
{
  switch (state) {
    case State::Idle:
      state = State::WaitType;
      successType = success;
      errorCb = error;
      rfid.transfer({{0x26}, 7}, std::bind(&Card::typeSuccess, this, std::placeholders::_1), std::bind(&Card::transferError, this));
      break;

    case State::Uninitialized:
    case State::WaitType:
    case State::WaitId:
      error();
      break;
  }
}

void Card::requestId(const SuccessId& success, const Error& error)
{
  switch (state) {
    case State::Idle:
      state = State::WaitId;
      successId = success;
      errorCb = error;
      rfid.transfer({{0x93, 0x20}, 16}, std::bind(&Card::idSuccess, this, std::placeholders::_1), std::bind(&Card::transferError, this));
      break;

    case State::Uninitialized:
    case State::WaitType:
    case State::WaitId:
      error();
      break;
  }

}

void Card::typeSuccess(const Mfrc522::CardTransfer& back)
{
  switch (state) {
    case State::WaitType:
      state = State::Idle;
      if (back.numberOfBits == 16) {
        successType(back.data.at(0));
      } else {
        errorCb();
      }
      break;

    case State::Uninitialized:
    case State::Idle:
    case State::WaitId:
      break;
  }
}

void Card::idSuccess(const Mfrc522::CardTransfer& back)
{
  switch (state) {
    case State::WaitId:
      state = State::Idle;
      if (back.numberOfBits != 40) {
        std::cout << "wrong number of bits: " << back.numberOfBits << std::endl;
        errorCb();
      } else {
        if (isChecksumOk(back.data)) {
          successId({back.data.begin(), back.data.begin()+back.data.size()-1});
        } else {
          std::cout << "wrong checksum" << std::endl;
          errorCb();
        }
      }
      break;

    case State::Uninitialized:
    case State::Idle:
    case State::WaitType:
      break;
  }
}

void Card::transferError()
{
  switch (state) {
    case State::WaitType:
    case State::WaitId:
      state = State::Idle;
      errorCb();
      break;

    case State::Uninitialized:
    case State::Idle:
      break;
  }
}

bool Card::isChecksumOk(const std::vector<uint8_t>& buffer)
{
  uint8_t serial_number_check = 0;
  for (const auto data : buffer) {
    serial_number_check = serial_number_check ^ data;
  }

  return serial_number_check == 0;
}


}
