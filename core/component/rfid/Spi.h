#pragma once

#include <cstdint>
#include <vector>

namespace core::component::rfid
{


class Spi
{
public:
  using Buffer = std::vector<uint8_t>;

  virtual void open(int bus, int device) = 0;
  virtual bool isOpen() const = 0;

  virtual void setMaxSpeedHz(uint32_t value) = 0;
  virtual std::vector<Buffer> transfer(const std::vector<Buffer>&) = 0;

};


}
