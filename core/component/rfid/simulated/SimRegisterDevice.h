/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "../RegisterDevice.h"
#include <map>

namespace core::component::rfid::simulated
{


class SimRegisterDevice :
    public core::component::rfid::RegisterDevice
{
public:
  using Registers = std::map<uint8_t, std::vector<uint8_t>>;

  bool isOpen() const override;
  bool isOpenResult{true};

  void open() override;
  void write(const RegisterSequence& seq) override;
  RegisterSequence read(const ReadRequestSequence& seq) override;

  bool canRead{true};

  Registers rxRegisters{};
  Registers txRegisters{};
};


}
