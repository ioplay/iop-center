/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "core/component/rfid/Spi.h"
#include <queue>


namespace core::component::rfid::simulated
{


class SimulatedSpi :
    public Spi
{
public:
  void open(int, int) override;

  bool isOpen() const override;

  void setMaxSpeedHz(uint32_t) override;

  std::vector<Buffer> transfer(const std::vector<Buffer>&) override;
  std::queue<std::vector<Buffer>> tx{};
  std::queue<std::vector<Buffer>> rc{};

};


}
