/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "SimRegisterDevice.h"

namespace core::component::rfid::simulated
{


bool SimRegisterDevice::isOpen() const
{
  return isOpenResult;
}

void SimRegisterDevice::open()
{
}

void SimRegisterDevice::write(const RegisterDevice::RegisterSequence& seq)
{
  for (const auto& reg : seq) {
    rxRegisters[reg.address] = reg.value;
  }
}

RegisterDevice::RegisterSequence SimRegisterDevice::read(const RegisterDevice::ReadRequestSequence& seq)
{
  if (!canRead) {
    return {};
  }

  RegisterSequence result{};

  for (const auto& req : seq) {
    const auto idx = txRegisters.find(req.address);
    if (idx == txRegisters.end()) {
      throw std::runtime_error("no value in register " + std::to_string(req.address));
    }
    result.push_back({req.address, idx->second});
  }

  return result;
}


}
