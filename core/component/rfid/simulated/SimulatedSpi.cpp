/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "SimulatedSpi.h"
#include <stdexcept>

namespace core::component::rfid::simulated
{


void SimulatedSpi::open(int, int)
{
}

bool SimulatedSpi::isOpen() const
{
  return {};
}

void SimulatedSpi::setMaxSpeedHz(uint32_t)
{
}

std::vector<Spi::Buffer> SimulatedSpi::transfer(const std::vector<Spi::Buffer>& txValue)
{
  tx.push(txValue);
  if (rc.empty()) {
    throw std::runtime_error("no receive data available");
  }
  const auto rcValue = rc.front();
  rc.pop();
  return rcValue;
}


}
