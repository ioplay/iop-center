#pragma once

#include <cstdint>
#include <vector>

namespace core::component::rfid
{


class RegisterDevice
{
public:
  virtual ~RegisterDevice() = default;

  struct Register
  {
    Register(uint8_t address_, uint8_t value_) :
      address{address_},
      value{value_}
    {
    }

    Register(uint8_t address_, std::vector<uint8_t> value_) :
      address{address_},
      value{value_}
    {
    }

    uint8_t address;
    std::vector<uint8_t> value;

    bool operator==(const Register& rhs) const
    {
      return
          (address == rhs.address) &&
          (value == rhs.value);
    }
  };
  using RegisterSequence = std::vector<Register>;

  struct ReadRequest
  {
    ReadRequest(uint8_t address_) :
      address{address_},
      count{1}
    {
    }

    ReadRequest(uint8_t address_, std::size_t count_) :
      address{address_},
      count{count_}
    {
    }

    uint8_t address;
    std::size_t count;
  };
  using ReadRequestSequence = std::vector<ReadRequest>;

  virtual bool isOpen() const = 0;

  virtual void open() = 0;
  virtual void write(const RegisterSequence&) = 0;
  virtual RegisterSequence read(const ReadRequestSequence&) = 0;

};


}
