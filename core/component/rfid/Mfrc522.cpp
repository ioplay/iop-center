#include "Mfrc522.h"

#include "RegisterDevice.h"
#include "core/util/Timer.h"
#include <algorithm>
#include <cassert>
#include <iostream>
#include <thread>

namespace core::component::rfid
{
namespace
{


enum Mode
{
  mode_idle = 0x00,
  mode_transrec = 0x0C,
  mode_reset = 0x0F,
};

enum Registers
{
  reg_CommandReg = 0x01,
  reg_ComIrqReg = 0x04,
  reg_ErrorReg = 0x06,
  reg_FIFODataReg = 0x09,
  reg_FIFOLevelReg = 0x0A,
  reg_ControlReg = 0x0C,
  reg_BitFramingReg = 0x0D,
  reg_TxControlReg = 0x14,
  reg_TxASKReg = 0x15,
};


}


Mfrc522::Mfrc522(RegisterDevice& device_, util::Timer& timer_) :
  device{device_},
  timer{timer_}
{
}

void Mfrc522::init()
{
  const RegisterDevice::RegisterSequence sequence {
    {reg_CommandReg, mode_reset},
    {reg_TxASKReg, 0x40},
    {reg_TxControlReg, 0x83}, // enable antenna
  };

  device.write(sequence);

  state = State::Idle;
}

void Mfrc522::transfer(const CardTransfer& data, const Mfrc522::Success& success, const Mfrc522::Error& error)
{
  switch (state) {
    case State::Idle:
    {
      assert(data.data.size() == (data.numberOfBits+1)/8);

      this->success = success;
      this->error = error;

      const uint8_t valueBitFramingReg = data.numberOfBits & 0x07;
      device.write({
        {reg_FIFOLevelReg, 0x80}, // clear FIFO
        {reg_CommandReg, mode_idle}, // cancel transmission, clears also the StartSend bit
        {reg_ComIrqReg, 0x7f}, // clear interrupt flags
        {reg_FIFODataReg, data.data},
        {reg_CommandReg, mode_transrec},
        {reg_BitFramingReg, uint8_t(valueBitFramingReg | 0x80)},
      });

      state = State::Transmitting;
      timer.startSingle(std::chrono::milliseconds(2), std::bind(&Mfrc522::transmitTimeout, this));
      break;
    }

    default:
      error();
      break;
  }
}

void Mfrc522::transmitTimeout()
{
  switch (state) {
    case State::Transmitting:
    {
      const uint8_t FifoSize = 64;
      const auto result = device.read({
        {reg_ComIrqReg},
        {reg_ErrorReg},
        {reg_FIFOLevelReg},
        {reg_ControlReg},
        {reg_FIFODataReg, FifoSize},
      });

      if (result.size() != 5) {
        state = State::Idle;
        error();
      } else {
        const auto irqReg = result.at(0).value.at(0);
        const auto errorValue = result.at(1).value.at(0);

        if (!hasDataReceived(irqReg, errorValue)) {
          state = State::Idle;
          error();
        } else {
          const auto n = result.at(2).value.at(0);
          const auto controlRegValue = result.at(3).value.at(0);
          const auto recv = result.at(4).value;

          const auto last_bits = controlRegValue & 0x07;

          std::size_t back_length{0};
          if (last_bits == 0) {
            back_length = n * 8;
          } else {
            back_length = (n - 1) * 8 + last_bits;
          }
          const auto count = std::min(std::max(uint8_t(1), n), uint8_t(16));

          const std::vector<uint8_t> back_data{recv.begin(), recv.begin()+count};

          state = State::Idle;
          success({back_data, back_length});
        }
      }
      break;
    }

    case State::Uninitialized:
    case State::Idle:
      break;
  }
}

bool Mfrc522::hasDataReceived(uint8_t irq, uint8_t error)
{
  if ((irq & 0x20) == 0x00) {
    return false;
  }

  if ((error & 0x1B) != 0x00) {
    return false;
  }

  return true;
}


}
