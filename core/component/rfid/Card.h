#pragma once

#include "Mfrc522.h"
#include <vector>
#include <optional>
#include <functional>

namespace core::component::rfid
{


//TODO split into 2 sequences
class Card
{
public:
  Card(Mfrc522&);

  using SuccessType = std::function<void(uint8_t)>;
  using SuccessId = std::function<void(const std::vector<uint8_t>&)>;
  using Error = std::function<void()>;

  void init();
  void requestType(const SuccessType&, const Error&);
  void requestId(const SuccessId&, const Error&);

private:
  Mfrc522& rfid;

  enum class State
  {
    Uninitialized,
    Idle,
    WaitType,
    WaitId,
  };
  State state{State::Uninitialized};

  SuccessType successType;
  SuccessId successId;
  Error errorCb;

  void typeSuccess(const Mfrc522::CardTransfer&);
  void idSuccess(const Mfrc522::CardTransfer&);
  void transferError();

  static bool isChecksumOk(const std::vector<uint8_t>&) ;
};


}
