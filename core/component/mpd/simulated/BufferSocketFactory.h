/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "core/component/mpd/SocketFactory.h"
#include <QMap>

namespace core::component::mpd::simulated
{

class BufferSocket;


class BufferSocketFactory :
    public SocketFactory
{
public:
  Socket* create() override;
  void dispose(Socket*) override;

  BufferSocket* get(const QString& name);

  QString read(const QString& name) const;
  void clear(const QString& name);
  void write(const QString& name, const QString& data) const;

private:
  QList<BufferSocket*> sockets{};

  BufferSocket* find(const QString& name);

};


}
