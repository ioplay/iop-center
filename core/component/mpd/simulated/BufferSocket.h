/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "core/component/mpd/Socket.h"
#include "RcTxBuffer.h"

namespace core::component::mpd::simulated
{


class BufferSocket :
    public Socket
{
public:
  void connectToServer(const QString& name, QIODevice::OpenMode openMode) override;
  void disconnectFromServer() override;

  RcTxBuffer* getDevice() override;

  QString getName() const;

private:
  QString name{};
  RcTxBuffer buffer{};

};


}
