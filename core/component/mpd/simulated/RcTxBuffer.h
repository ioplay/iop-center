/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "core/component/mpd/Socket.h"
#include <QBuffer>

namespace core::component::mpd::simulated
{


class RcTxBuffer :
    public QIODevice
{
public:
  bool canReadLine() const override;
  bool reset() override;

  QString read();
  void write(const QString&);

protected:
  qint64 readData(char* data, qint64 maxSize) override;
  qint64 writeData(const char* data, qint64 maxSize) override;

private:
  QString txBuffer{};
  QString rcBuffer{};
};


}
