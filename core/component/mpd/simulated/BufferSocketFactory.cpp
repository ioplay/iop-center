/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "BufferSocketFactory.h"
#include "BufferSocket.h"
#include <QTextStream>

namespace core::component::mpd::simulated
{


Socket* BufferSocketFactory::create()
{
  auto* socket = new BufferSocket();
  sockets.push_back(socket);
  return socket;
}

void BufferSocketFactory::dispose(Socket* value)
{
  auto socket = dynamic_cast<BufferSocket*>(value);
  if (socket != value) {
    throw std::invalid_argument("wrong socket type");
  }

  sockets.removeAll(socket);
  delete socket;
}

void BufferSocketFactory::clear(const QString& name)
{
  get(name)->getDevice()->reset();
}

QString BufferSocketFactory::read(const QString& name) const
{
  BufferSocket* socket = const_cast<BufferSocketFactory*>(this)->get(name);
  return socket->getDevice()->read();
}

void BufferSocketFactory::write(const QString& name, const QString& data) const
{
  BufferSocket* socket = const_cast<BufferSocketFactory*>(this)->get(name);
  socket->getDevice()->write(data);
}

BufferSocket* BufferSocketFactory::find(const QString& name)
{
  for (const auto socket : sockets) {
    if (socket->getName() == name) {
      return socket;
    }
  }

  return {};
}

BufferSocket* BufferSocketFactory::get(const QString& name)
{
  const auto result = find(name);

  if (!result) {
    throw std::invalid_argument("device does not exists: " + name.toStdString());
  }

  return result;
}


}
