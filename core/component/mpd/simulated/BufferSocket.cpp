/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "BufferSocket.h"

namespace core::component::mpd::simulated
{


void BufferSocket::connectToServer(const QString& name, QIODevice::OpenMode openMode)
{
  this->name = name;
  buffer.open(openMode);
  connected();
}

void BufferSocket::disconnectFromServer()
{
  buffer.close();
  disconnected();
}

QString BufferSocket::getName() const
{
  return name;
}

RcTxBuffer* BufferSocket::getDevice()
{
  return &buffer;
}


}
