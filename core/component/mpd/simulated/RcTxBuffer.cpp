/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "RcTxBuffer.h"

namespace core::component::mpd::simulated
{


bool RcTxBuffer::canReadLine() const
{
  return rcBuffer.contains('\n') || QIODevice::canReadLine();
}

bool RcTxBuffer::reset()
{
  const auto result = QIODevice::reset();
  txBuffer.clear();
  rcBuffer.clear();
  return result;
}

QString RcTxBuffer::read()
{
  const auto result = txBuffer;
  txBuffer.clear();
  return result;
}

void RcTxBuffer::write(const QString& data)
{
  rcBuffer += data;
  readyRead();
}

qint64 RcTxBuffer::readData(char* data, qint64 maxSize)
{
  const auto raw = rcBuffer.toStdString();
  maxSize = std::min(maxSize, static_cast<qint64>(raw.size()));
  memcpy(data, raw.c_str(), maxSize);
  rcBuffer.remove(0, maxSize);
  return maxSize;
}

qint64 RcTxBuffer::writeData(const char* data, qint64 maxSize)
{
  const auto newData = QString::fromStdString(std::string{data, data+maxSize});
  txBuffer += newData;
  return maxSize;
}


}
