/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Command.h"

namespace core::component::mpd
{


class Sender
{
public:
  virtual ~Sender() = default;

  virtual void send(const Command&) = 0;
};


}
