/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "EventHandler.h"
#include "CommandList.h"
#include "Encode.h"
#include "Transmitter.h"

namespace core::component::mpd
{


EventHandler::EventHandler(Transmitter& transmitter_) :
  transmitter{transmitter_}
{
}

void EventHandler::handle(const event::player::Play& event)
{
  CommandList commands{};

  commands.add(Encode::clear());
  if (!event.playlist.empty()) {
    for (const auto& uri : event.playlist) {
      commands.add(Encode::add(uri));
    }
    commands.add(Encode::play());
  }

  transmitter.send(commands.get());
}

void EventHandler::handle(const event::player::Continue&)
{
  transmitter.send(Encode::pause(false));
}

void EventHandler::handle(const event::player::Pause&)
{
  transmitter.send(Encode::pause(true));
}


}
