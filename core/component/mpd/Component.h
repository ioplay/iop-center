/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Receiver.h"
#include "Connection.h"
#include "EventHandler.h"
#include "StreamSender.h"
#include "Transmitter.h"
#include "event/event.h"
#include "core/base/Component.h"

namespace core::component::mpd
{

class SocketFactory;


/*
 * Serialization of MPD protocol. See:
 *   https://www.musicpd.org/doc/html/protocol.html
 * For tests use:
 *   socat - UNIX-CONNECT:/var/run/mpd/socket
 */
class Component :
    public base::Component
{
public:
  Component(SocketFactory&);
  ~Component() override;

  void initialize() override;
  void deinitialize() override;

  void recvEvent(const event::SharedEvent&) override;

private:
  SocketFactory& streamFactory;
  Socket* socket;
  StreamSender sender;
  Transmitter transmitter;
  Receiver receiver;
  Connection connection;
  EventHandler eventHandler;
};


}
