#pragma once

#include "core/base/ComponentFactory.h"

namespace core::component::mpd
{

class SocketFactory;


class Factory :
    public base::ComponentFactory
{
public:
  Factory(SocketFactory&);

  base::Component* produce() override;

private:
  SocketFactory& streamFactory;

};


}
