/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Encode.h"

namespace core::component::mpd
{


Command Encode::clear()
{
  return "clear\n";
}

Command Encode::add(const QUrl& uri)
{
  Q_ASSERT_X(uri.isValid(), __func__, uri.errorString().toStdString().c_str());
  auto encoded = uri.isLocalFile() ? ("file://" + uri.toLocalFile()) : uri.toString();
  encoded.replace("\\", "\\\\");
  encoded.replace("\"", "\\\"");
  return "add \"" + encoded + "\"\n";
}

Command Encode::play()
{
  return "play\n";
}

Command Encode::pause(bool value)
{
  const QString arg = value ? "1" : "0";
  return "pause " + arg + "\n";
}

Command Encode::commandListBegin()
{
  return "command_list_begin\n";
}

Command Encode::commandListEnd()
{
  return "command_list_end\n";
}

Command Encode::idle()
{
  return "idle\n";
}

Command Encode::noIdle()
{
  return "noidle\n";
}


}
