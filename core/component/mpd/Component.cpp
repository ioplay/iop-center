/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Component.h"
#include "core/component/mpd/Socket.h"
#include "core/component/mpd/SocketFactory.h"

namespace core::component::mpd
{


Component::Component(SocketFactory& streamFactory_) :
  streamFactory{streamFactory_},
  socket{streamFactory.create()},
  sender{socket->getDevice()},
  transmitter{sender},
  receiver{socket, transmitter},
  connection{socket},
  eventHandler{transmitter}
{
  connect(&transmitter, &Transmitter::connected, this, &Component::initializeComplete);
  connect(&connection, &Connection::deinitializeComplete, this, &Component::deinitializeComplete);
}

Component::~Component()
{
  streamFactory.dispose(socket);
}

void Component::initialize()
{
  connection.initialize();
}

void Component::deinitialize()
{
  connection.deinitialize();
}

void Component::recvEvent(const event::SharedEvent& event)
{
  event::dispatch<event::player::Player, event::player::Handler>(*event, eventHandler);
}


}
