#include "Factory.h"
#include "Component.h"

namespace core::component::mpd
{


Factory::Factory(SocketFactory& streamFactory_) :
  streamFactory{streamFactory_}
{
}

base::Component* Factory::produce()
{
  return new Component(streamFactory);
}


}
