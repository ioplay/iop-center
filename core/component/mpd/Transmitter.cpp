/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Transmitter.h"
#include "Encode.h"
#include "Sender.h"

namespace core::component::mpd
{


Transmitter::Transmitter(Sender& sender_) :
  sender{sender_}
{
}

void Transmitter::recveived(const QString& line)
{
  const auto endOfCommand = line.startsWith("OK") || line.startsWith("ACK");
  const auto newlyConnected = line.startsWith("OK MPD");
  if (endOfCommand || newlyConnected) {
    recvEndOfResponse();
  }
  if (newlyConnected) {
    connected();
  }
}

void Transmitter::recvEndOfResponse()
{
  if (commands.empty()) {
    inIdleMode = true;
    execute(Encode::idle());
  } else {
    inIdleMode = false;
    const auto command = commands.front();
    commands.pop_front();
    execute(command);
  }
}

void Transmitter::execute(const QString& command)
{
  sender.send(command);
}

void Transmitter::send(const Command& command)
{
  commands.append(command);

  if (inIdleMode) {
    inIdleMode = false;
    execute(Encode::noIdle());
  }
}


}
