/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <QObject>

namespace core::component::mpd
{

class Socket;
class Transmitter;


class Receiver :
    public QObject
{
  Q_OBJECT

public:
  Receiver(Socket*, Transmitter&);

private:
  Socket* const socket;
  Transmitter& transmitter;

  void readyRead();

};


}
