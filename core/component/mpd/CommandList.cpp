/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "CommandList.h"
#include "Encode.h"

namespace core::component::mpd
{


void CommandList::add(const Command& value)
{
  commands.push_back(value);
}

Command CommandList::get() const
{
  if (commands.size() == 1) {
    return commands.first();
  } else {
    QString result{};
    result += Encode::commandListBegin();
    result += commands.join("");
    result += Encode::commandListEnd();
    return result;
  }
}


}
