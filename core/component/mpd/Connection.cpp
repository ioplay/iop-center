/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Connection.h"
#include "Socket.h"

namespace core::component::mpd
{


Connection::Connection(Socket* socket_) :
  socket{socket_}
{
  QObject::connect(socket, &Socket::connected, this, &Connection::connected);
  QObject::connect(socket, &Socket::disconnected, this, &Connection::disconnected, Qt::QueuedConnection); // QLocalSocket has a problem when conencting while in disconnect slot
}

void Connection::initialize()
{
  state = State::Initializing;
  connect();
}

void Connection::connected()
{
  switch (state) {
    case State::Initializing:
      state = State::Running;
      break;

    case State::Uninitialized:
    case State::Running:
    case State::Deinitializing:
      break;
  }
}

void Connection::deinitialize()
{
  state = State::Deinitializing;
  disconnect();
}

void Connection::disconnected()
{
  switch (state) {
    case State::Running:
      connect();
      break;

    case State::Deinitializing:
      state = State::Uninitialized;
      deinitializeComplete();
      break;

    case State::Uninitialized:
    case State::Initializing:
      break;
  }
}

void Connection::connect()
{
  socket->connectToServer("/var/run/mpd/socket", QIODevice::OpenModeFlag::ReadWrite);
}

void Connection::disconnect()
{
  socket->disconnectFromServer();
}


}
