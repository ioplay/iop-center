/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <QIODevice>
#include <QObject>

namespace core::component::mpd
{


class Socket :
    public QObject
{
  Q_OBJECT

public:
  virtual void connectToServer(const QString &name, QIODevice::OpenMode openMode) = 0;
  virtual void disconnectFromServer() = 0;

  virtual QIODevice* getDevice() = 0;

signals:
  void connected();
  void disconnected();

};


}
