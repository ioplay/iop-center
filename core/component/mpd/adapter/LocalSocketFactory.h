/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "core/component/mpd/SocketFactory.h"

namespace core::component::mpd::adapter
{


class LocalSocketFactory :
    public SocketFactory
{
public:
  Socket* create() override;
  void dispose(Socket*) override;

};


}
