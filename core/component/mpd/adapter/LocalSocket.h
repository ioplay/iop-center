/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "core/component/mpd/Socket.h"
#include <QLocalSocket>

namespace core::component::mpd::adapter
{


class LocalSocket :
    public Socket
{
public:
  LocalSocket();

  void connectToServer(const QString& name, QIODevice::OpenMode openMode) override;
  void disconnectFromServer() override;

  QIODevice* getDevice() override;

private:
  QLocalSocket socket;

};


}
