/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "LocalSocket.h"
#include <QDebug>

namespace core::component::mpd::adapter
{


LocalSocket::LocalSocket()
{
  connect(&socket, &QLocalSocket::connected, this, &Socket::connected);
  connect(&socket, &QLocalSocket::disconnected, this, &Socket::disconnected);
  connect(&socket, QOverload<QLocalSocket::LocalSocketError>::of(&QLocalSocket::error), [](QLocalSocket::LocalSocketError socketError){
    qDebug() << "LocalSocket error:" << socketError;
  });
}

void LocalSocket::connectToServer(const QString& name, QIODevice::OpenMode openMode)
{
  socket.connectToServer(name, openMode);
}

void LocalSocket::disconnectFromServer()
{
  socket.disconnectFromServer();
}

QIODevice* LocalSocket::getDevice()
{
  return &socket;
}


}
