/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "LocalSocketFactory.h"
#include "LocalSocket.h"

namespace core::component::mpd::adapter
{


Socket* LocalSocketFactory::create()
{
  const auto socket = new LocalSocket();
  return socket;
}

void LocalSocketFactory::dispose(Socket* socket)
{
  delete socket;
}


}
