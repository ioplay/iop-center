/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "../Socket.h"
#include "core/component/mpd/simulated/RcTxBuffer.h"

namespace core::component::mpd::test
{


class DummySocket :
    public Socket
{
public:
  void connectToServer(const QString& name, QIODevice::OpenMode) override;
  QString connectName{};

  void disconnectFromServer() override;
  bool disconnectCalled{};

  simulated::RcTxBuffer* getDevice() override;

private:
  simulated::RcTxBuffer buffer;

};


}
