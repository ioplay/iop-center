/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "core/component/mpd/Sender.h"
#include <QList>

namespace core::component::mpd::test
{


class DummySender :
    public Sender
{
public:
  void send(const Command& command);

  QList<Command> commands{};
};


}
