/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "core/component/mpd/Transmitter.h"
#include "DummySender.h"
#include "core/component/mpd/Encode.h"
#include <QSignalSpy>
#include <gtest/gtest.h>


namespace core::component::mpd::test
{
namespace
{


class mpd_Transmitter_Test :
    public testing::Test
{
public:
  DummySender sender{};
  Transmitter testee{sender};

  QSignalSpy connectedSpy{&testee, &Transmitter::connected};

};

using C = QList<Command>;


TEST_F(mpd_Transmitter_Test, goes_into_idle_mode_when_connected)
{

  testee.recveived("OK MPD 0.21.11");

  ASSERT_EQ(C{Encode::idle()}, sender.commands);
}

TEST_F(mpd_Transmitter_Test, leaves_idle_mode_when_received_a_command_to_send)
{
  testee.recveived("OK MPD 0.21.11");
  sender.commands.clear();

  testee.send("hello");

  ASSERT_EQ(C{Encode::noIdle()}, sender.commands);
}

TEST_F(mpd_Transmitter_Test, sends_command_after_idle_mode_is_left)
{
  testee.recveived("OK MPD 0.21.11");
  testee.send("hello");
  sender.commands.clear();

  testee.recveived("OK");

  ASSERT_EQ(C{"hello"}, sender.commands);
}

TEST_F(mpd_Transmitter_Test, goes_into_idle_mode_after_command_is_sent)
{
  testee.recveived("OK MPD 0.21.11");
  testee.send("hello");
  testee.recveived("OK");
  sender.commands.clear();

  testee.recveived("OK");

  ASSERT_EQ(C{Encode::idle()}, sender.commands);
}

TEST_F(mpd_Transmitter_Test, does_nothing_when_receiving_a_command_to_send_while_waiting_to_leave_idle_mode)
{
  testee.recveived("OK MPD 0.21.11");
  testee.send("hello");
  sender.commands.clear();

  testee.send("world");

  ASSERT_EQ(C{}, sender.commands);
}

TEST_F(mpd_Transmitter_Test, send_first_command_when_leaving_idle_after_receiving_two_commands_to_send)
{
  testee.recveived("OK MPD 0.21.11");
  testee.send("hello");
  testee.send("world");
  sender.commands.clear();

  testee.recveived("OK");

  ASSERT_EQ(C{"hello"}, sender.commands);
}

TEST_F(mpd_Transmitter_Test, send_second_command_after_ok_of_first_command_after_receiving_two_commands_to_send)
{
  testee.recveived("OK MPD 0.21.11");
  testee.send("hello");
  testee.send("world");
  testee.recveived("OK");
  sender.commands.clear();

  testee.recveived("OK");

  ASSERT_EQ(C{"world"}, sender.commands);
}

TEST_F(mpd_Transmitter_Test, entering_idle_mode_after_sending_two_commands)
{
  testee.recveived("OK MPD 0.21.11");
  testee.send("hello");
  testee.send("world");
  testee.recveived("OK");
  testee.recveived("OK");
  sender.commands.clear();

  testee.recveived("OK");

  ASSERT_EQ(C{Encode::idle()}, sender.commands);
}

TEST_F(mpd_Transmitter_Test, does_nothing_when_receiving_a_command_to_send_while_waiting_for_response_of_previous_command)
{
  testee.recveived("OK MPD 0.21.11");
  testee.send("hello");
  testee.recveived("OK");
  sender.commands.clear();

  testee.send("world");

  ASSERT_EQ(C{}, sender.commands);
}

TEST_F(mpd_Transmitter_Test, send_second_command_after_receiving_response_of_first_command)
{
  testee.recveived("OK MPD 0.21.11");
  testee.send("hello");
  testee.recveived("OK");
  testee.send("world");
  sender.commands.clear();

  testee.recveived("OK");

  ASSERT_EQ(C{"world"}, sender.commands);
}

TEST_F(mpd_Transmitter_Test, goes_into_idle_when_receiving_OK_while_supposedly_in_idle)
{
  testee.recveived("OK MPD 0.21.11");
  sender.commands.clear();

  testee.recveived("OK");

  ASSERT_EQ(C{Encode::idle()}, sender.commands);
}

TEST_F(mpd_Transmitter_Test, goes_into_idle_mode_after_error)
{
  testee.recveived("OK MPD 0.21.11");
  testee.send("hello");
  testee.recveived("OK");
  sender.commands.clear();

  testee.recveived("ACK [50@1] {play} song doesn't exist: \"10240\"");

  ASSERT_EQ(C{Encode::idle()}, sender.commands);
}

TEST_F(mpd_Transmitter_Test, notify_when_connected)
{

  testee.recveived("OK MPD 0.21.11");

  ASSERT_EQ(1, connectedSpy.count());
}

TEST_F(mpd_Transmitter_Test, does_not_notify_connected_on_successfull_command)
{

  testee.recveived("OK");

  ASSERT_EQ(0, connectedSpy.count());
}

TEST_F(mpd_Transmitter_Test, does_not_notify_connected_on_unsuccessfull_command)
{

  testee.recveived("ACK");

  ASSERT_EQ(0, connectedSpy.count());
}


}
}
