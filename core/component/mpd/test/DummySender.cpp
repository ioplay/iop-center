/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DummySender.h"

namespace core::component::mpd::test
{


void DummySender::send(const Command& command)
{
  commands << command;
}


}
