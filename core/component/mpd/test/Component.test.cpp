/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "../Component.h"
#include "DummySocketFactory.h"
#include <QCoreApplication>
#include <QSignalSpy>
#include <gtest/gtest.h>


namespace core::component::mpd::test
{
namespace
{


class mpd_Component_Test :
    public testing::Test
{
public:
  int argc{1};
  char* argv{"-test-"};
  QCoreApplication app{argc, &argv};
  DummySocketFactory socketFactory{};
  Component testee{socketFactory};
  QSignalSpy initializeCompleteSpy{&testee, &Component::initializeComplete};
  QSignalSpy deinitializeCompleteSpy{&testee, &Component::deinitializeComplete};

  void SetUp() override
  {
    ASSERT_TRUE(initializeCompleteSpy.isValid());
    ASSERT_TRUE(deinitializeCompleteSpy.isValid());
  }

};


TEST_F(mpd_Component_Test, connects_to_local_mpd_when_initializing)
{

  testee.initialize();

  ASSERT_EQ("/var/run/mpd/socket", socketFactory.getSocket().connectName.toStdString());
}

TEST_F(mpd_Component_Test, is_not_initialized_when_socket_is_not_yet_connected)
{

  testee.initialize();

  ASSERT_EQ(0, initializeCompleteSpy.size());
}

TEST_F(mpd_Component_Test, is_not_initialized_when_mpd_has_not_yet_responded)
{
  testee.initialize();

  socketFactory.getSocket().connected();

  ASSERT_EQ(0, initializeCompleteSpy.size());
}

TEST_F(mpd_Component_Test, is_initialized_when_mpd_respondend)
{
  testee.initialize();
  socketFactory.getSocket().connected();

  socketFactory.getSocket().getDevice()->write("OK MPD 0.21.11\n");

  ASSERT_EQ(1, initializeCompleteSpy.size());
}

TEST_F(mpd_Component_Test, disconnects_when_deinitializing)
{
  testee.initialize();
  socketFactory.getSocket().connected();

  testee.deinitialize();

  ASSERT_TRUE(socketFactory.getSocket().disconnectCalled);
}

TEST_F(mpd_Component_Test, is_deinitialized_when_socket_is_disconnected)
{
  testee.initialize();
  socketFactory.getSocket().connected();
  testee.deinitialize();

  socketFactory.getSocket().disconnected();
  app.processEvents();

  ASSERT_EQ(1, deinitializeCompleteSpy.size());
}

TEST_F(mpd_Component_Test, does_not_reconnects_synchron_when_disconnected_unexpectedly_since_this_does_not_work_with_QLocalSocket)
{
  testee.initialize();
  socketFactory.getSocket().connected();
  socketFactory.getSocket().connectName.clear();

  socketFactory.getSocket().disconnected();

  ASSERT_EQ("", socketFactory.getSocket().connectName.toStdString());
}

TEST_F(mpd_Component_Test, reconnects_asynchronous_when_disconnected_unexpectedly)
{
  testee.initialize();
  socketFactory.getSocket().connected();
  socketFactory.getSocket().connectName.clear();

  socketFactory.getSocket().disconnected();
  app.processEvents();

  ASSERT_EQ("/var/run/mpd/socket", socketFactory.getSocket().connectName.toStdString());
}

TEST_F(mpd_Component_Test, does_not_notify_when_reconnected)
{
  testee.initialize();
  socketFactory.getSocket().connected();
  socketFactory.getSocket().disconnected();
  app.processEvents();
  initializeCompleteSpy.clear();

  socketFactory.getSocket().connected();

  ASSERT_EQ(0, initializeCompleteSpy.size());
}


}
}
