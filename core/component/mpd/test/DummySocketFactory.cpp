/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DummySocketFactory.h"


namespace core::component::mpd::test
{


Socket* DummySocketFactory::create()
{
  if (socket) {
    throw std::runtime_error("this implementation can only manage 1 socket at the time");
  }

  socket = new DummySocket();
  return socket;
}

void DummySocketFactory::dispose(Socket* value)
{
  if (socket != value) {
    throw std::invalid_argument("disposing invalid socket");
  }

  delete socket;
  socket = {};
}

bool DummySocketFactory::hasSocket() const
{
  return socket;
}

DummySocket& DummySocketFactory::getSocket()
{
  if (!socket) {
    throw std::runtime_error("socket not available");
  }

  return *socket;
}


}
