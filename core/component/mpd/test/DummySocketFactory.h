/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "../SocketFactory.h"
#include "DummySocket.h"


namespace core::component::mpd::test
{


class DummySocketFactory :
    public SocketFactory
{
public:
  Socket* create() override;
  void dispose(Socket*) override;

  bool hasSocket() const;
  DummySocket& getSocket();

private:
  DummySocket* socket{};

};


}
