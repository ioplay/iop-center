/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "core/component/mpd/Encode.h"
#include <gtest/gtest.h>


namespace core::component::mpd::test
{
namespace
{


TEST(mpd_Encode_Test, encode_double_quote)
{
  const QUrl url{"file:///special1_\".mp3"};

  const auto actual = Encode::add(url);

  ASSERT_EQ("add \"file:///special1_\\\".mp3\"\n", actual.toStdString());
}

TEST(mpd_Encode_Test, encode_single_quote)
{
  const QUrl url{"file:///special2_'.mp3"};

  const auto actual = Encode::add(url);

  ASSERT_EQ("add \"file:///special2_'.mp3\"\n", actual.toStdString());
}

TEST(mpd_Encode_Test, encode_backslash)
{
  const QUrl url{"file:///special3_\\.mp3"};

  const auto actual = Encode::add(url);

  ASSERT_EQ("add \"file:///special3_\\\\.mp3\"\n", actual.toStdString());
}

TEST(mpd_Encode_Test, can_encode_https_url)
{
  const QUrl url{"https://stream.radioparadise.com/rp_192m.ogg"};

  const auto actual = Encode::add(url);

  ASSERT_EQ("add \"https://stream.radioparadise.com/rp_192m.ogg\"\n", actual.toStdString());
}


}
}
