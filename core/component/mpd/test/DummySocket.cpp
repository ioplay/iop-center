/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "DummySocket.h"

namespace core::component::mpd::test
{


void DummySocket::connectToServer(const QString& name, QIODevice::OpenMode mode)
{
  connectName = name;
  buffer.open(mode);
}

void DummySocket::disconnectFromServer()
{
  buffer.close();
  disconnectCalled = true;
}

simulated::RcTxBuffer* DummySocket::getDevice()
{
  return &buffer;
}


}
