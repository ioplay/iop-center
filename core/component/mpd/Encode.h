/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Command.h"
#include <QUrl>

namespace core::component::mpd
{


class Encode
{
public:
  static Command clear();
  static Command add(const QUrl&);
  static Command play();
  static Command pause(bool);

  static Command commandListBegin();
  static Command commandListEnd();
  static Command idle();
  static Command noIdle();

};


}
