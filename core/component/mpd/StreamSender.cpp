/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "StreamSender.h"

namespace core::component::mpd
{


StreamSender::StreamSender(QIODevice* device) :
  stream{device}
{
}

void StreamSender::send(const Command& command)
{
  stream << command;
  stream.flush();
}


}
