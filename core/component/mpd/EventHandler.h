/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "event/player.h"

namespace core::component::mpd
{

class Transmitter;


class EventHandler :
    public event::player::Handler
{
public:
  EventHandler(Transmitter&);

  void handle(const event::player::Play&) override;
  void handle(const event::player::Pause&) override;
  void handle(const event::player::Continue&) override;

private:
  Transmitter& transmitter;

};


}
