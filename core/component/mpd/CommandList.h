/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Command.h"
#include <QList>

namespace core::component::mpd
{


class CommandList
{
public:
  void add(const Command&);
  Command get() const;

private:
  QList<Command> commands{};
};


}
