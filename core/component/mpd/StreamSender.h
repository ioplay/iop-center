/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Sender.h"
#include <QTextStream>

namespace core::component::mpd
{


class StreamSender :
    public Sender
{
public:
  StreamSender(QIODevice*);

  void send(const Command&) override;

private:
  QTextStream stream{};

};


}
