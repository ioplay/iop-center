/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Receiver.h"
#include "Socket.h"
#include "Transmitter.h"

namespace core::component::mpd
{


Receiver::Receiver(Socket* socket_, Transmitter& transmitter_) :
  socket{socket_},
  transmitter{transmitter_}
{
  QObject::connect(socket->getDevice(), &QIODevice::readyRead, this, &Receiver::readyRead);
}

void Receiver::readyRead()
{
  while (socket->getDevice()->canReadLine()) {
    const auto line = socket->getDevice()->readLine();
    transmitter.recveived(line);
  }
}


}
