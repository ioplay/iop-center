/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <QObject>

namespace core::component::mpd
{

class Socket;


class Connection :
    public QObject
{
  Q_OBJECT

public:
  Connection(Socket*);

  void initialize();
  void deinitialize();

signals:
  void deinitializeComplete();

private:
  Socket* const socket{};

  enum class State
  {
    Uninitialized,
    Initializing,
    Running,
    Deinitializing,
  };
  State state{State::Uninitialized};

  void connect();
  void disconnect();

  void connected();
  void disconnected();

};


}
