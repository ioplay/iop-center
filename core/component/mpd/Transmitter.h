/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "Command.h"
#include <QString>
#include <QList>
#include <QObject>

namespace core::component::mpd
{

class Sender;


class Transmitter :
    public QObject
{
  Q_OBJECT

public:
  Transmitter(Sender&);

  void recveived(const QString& line);
  void send(const Command&);

signals:
  void connected();

private:
  Sender& sender;
  QList<Command> commands{};

  bool inIdleMode{false};

  void recvEndOfResponse();
  void execute(const QString&);

};


}
