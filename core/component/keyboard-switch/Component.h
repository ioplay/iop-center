/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "core/base/Component.h"
#include "event/switch.h"
#include <QSocketNotifier>
#include <string>
#include <QSet>

namespace core::component::keyboard_switch
{


class Component :
    public base::Component
{
public:
  Component();

  void initialize() override;

private:
  const int m_fd;
  QSocketNotifier* notifier{};

  void fileChanged();
  void received(const std::string&);
  void receivedLine(const QString&);

  std::string receivedData{};
  QSet<QString> switchesOn{};

};


}
