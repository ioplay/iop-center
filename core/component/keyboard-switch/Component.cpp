/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Component.h"
#include <QDebug>
#include <unistd.h>

namespace core::component::keyboard_switch
{


Component::Component() :
  m_fd{STDIN_FILENO}
{
}

void Component::initialize()
{
  notifier = new QSocketNotifier(m_fd, QSocketNotifier::Read, this);
  connect(notifier, &QSocketNotifier::activated, this, &Component::fileChanged);
  initializeComplete();
}

void Component::fileChanged()
{
  char buffer[20];
  const auto len = ::read(m_fd, buffer, sizeof(buffer));
  if (len <= 0) {
    return;
  }
  const std::string data{buffer, buffer+len};
  received(data);
}

void Component::received(const std::string& data)
{
  receivedData += data;

  const auto end = receivedData.find('\n');
  if (end == std::string::npos) {
    return;
  }

  const auto line = receivedData.substr(0, end);
  receivedData.erase(0, end+1);
  receivedLine(QString::fromStdString(line));
}

void Component::receivedLine(const QString& id)
{
  if (switchesOn.contains(id)) {
    qDebug() << "turn off" << id;
    switchesOn.remove(id);
    sendEvent(new event::switch_::Off(id));
  } else {
    qDebug() << "turn on" << id;
    switchesOn.insert(id);
    sendEvent(new event::switch_::On(id));
  }
  qDebug() << "active" << switchesOn.toList();
}


}
