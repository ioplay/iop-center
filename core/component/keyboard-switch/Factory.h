#pragma once

#include "core/base/ComponentFactory.h"

namespace core::component::keyboard_switch
{


class Factory :
    public base::ComponentFactory
{
public:
  base::Component* produce() override;

};


}
