#include "Factory.h"
#include "Component.h"

namespace core::component::keyboard_switch
{


base::Component* Factory::produce()
{
  return new Component();
}


}
