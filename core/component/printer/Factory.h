#pragma once

#include "core/base/ComponentFactory.h"
#include <ostream>

namespace core::component::printer
{


class Factory :
    public base::ComponentFactory
{
public:
  Factory(std::ostream&);

  base::Component* produce() override;

private:
  std::ostream& stream;

};


}
