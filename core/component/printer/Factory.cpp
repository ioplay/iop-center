#include "Factory.h"
#include "Component.h"

namespace core::component::printer
{


Factory::Factory(std::ostream& stream_) :
  stream{stream_}
{
}

base::Component* Factory::produce()
{
  return new Component(stream);
}


}
