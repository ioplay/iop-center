#pragma once

#include "event/event.h"
#include "core/base/Component.h"

namespace core::component::printer
{


class Component :
    public base::Component
{
public:
  Component(std::ostream&);

  void recvEvent(const event::SharedEvent&) override;

private:
  std::ostream& stream;

};


}
