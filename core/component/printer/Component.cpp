#include "Component.h"
#include <QJsonDocument>

namespace core::component::printer
{


Component::Component(std::ostream& stream_) :
  stream{stream_}
{
}

void Component::recvEvent(const event::SharedEvent& event)
{
  QString output{};
  output += "event " + (*event).name().join(".");
  output += "\n";
  const QJsonDocument doc{(*event).content()};
  output += doc.toJson();
  stream << output.toStdString();
}


}
