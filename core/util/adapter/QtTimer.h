/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "../Timer.h"
#include <QTimer>
#include <QMap>

namespace core::util::adapter
{


class QtTimer :
    public Timer
{
public:
  Id startCyclic(std::chrono::milliseconds, const Handler&) override;
  Id startSingle(std::chrono::milliseconds, const Handler&) override;
  void stop(Id) override;

private:
  Id nextId{1};

  enum class Type
  {
    Single,
    Cyclic,
  };

  QMap<Id, QTimer*> timers{};

  Id start(std::chrono::milliseconds, Type, const Handler&);
  Id generateId();

};


}
