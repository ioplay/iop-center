/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "QtTimer.h"

namespace core::util::adapter
{


Timer::Id QtTimer::startCyclic(std::chrono::milliseconds timeout, const Handler &handler)
{
  return start(timeout, Type::Cyclic, handler);
}

Timer::Id QtTimer::startSingle(std::chrono::milliseconds timeout, const Timer::Handler& handler)
{
  return start(timeout, Type::Single, handler);
}

Timer::Id QtTimer::start(std::chrono::milliseconds timeout, QtTimer::Type type, const Timer::Handler& handler)
{
  assert(timeout.count() > 0);

  const auto id = generateId();
  auto timer = new QTimer();
  timers[id] = timer;

  timer->setInterval(timeout);
  timer->setSingleShot(type == QtTimer::Type::Single);
  QObject::connect(timer, &QTimer::timeout, std::bind(handler, id));
  timer->start();

  return id;
}

void QtTimer::stop(Timer::Id id)
{
  auto timer = timers.value(id, {});
  if (timer) {
    timer->stop();
    timers.remove(id);
    timer->deleteLater();
  }
}

Timer::Id QtTimer::generateId()
{
  const auto id = nextId;

  //TODO handle overflow
  //TODO make sure we don't reuse a id that is still in use for a cyclic timer
  nextId++;

  return id;
}


}
