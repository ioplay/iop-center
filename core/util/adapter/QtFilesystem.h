#pragma once

#include "../Filesystem.h"

namespace core::util::adapter
{


class QtFilesystem :
    public Filesystem
{
public:
  QIODevice* getFile(const QString& filename) override;
  bool exists(const QString&) const override;
  bool isDirectory(const QString&) const override;
  QStringList listFiles(const QDir&) const override;
  QStringList listDirectories(const QDir&) const override;

};


}
