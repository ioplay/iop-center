#include "QtFilesystem.h"
#include <QDir>
#include <QFileInfo>
#include <fstream>

namespace core::util::adapter
{


QIODevice* QtFilesystem::getFile(const QString& filename)
{
  return new QFile(filename);
}

bool QtFilesystem::exists(const QString& path) const
{
  const QFileInfo fileInfo{path};
  const auto result = fileInfo.exists();
  return result;
}

bool QtFilesystem::isDirectory(const QString& path) const
{
  const QFileInfo fileInfo{path};
  const auto result = fileInfo.isDir();
  return result;
}

QStringList QtFilesystem::listFiles(const QDir& path) const
{
  return path.entryList(QDir::Files | QDir::NoDotAndDotDot);
}

QStringList QtFilesystem::listDirectories(const QDir& path) const
{
  return path.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
}


}
