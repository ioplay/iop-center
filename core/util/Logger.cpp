/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Logger.h"

#include <iomanip>
#include <iostream>
#include <sstream>

namespace core::util
{


Logger::Logger(const std::string& module_) :
  module{module_}
{
}

void Logger::inputTrace(const std::string& data)
{
//  std::cout << module << " in : " << data << std::endl;
}

void Logger::outputTrace(const std::string& data)
{
//  std::cout << module << " out: " << data << std::endl;
}

void Logger::inputTrace(const std::vector<uint8_t> &data)
{
  inputTrace(toString(data));
}

void Logger::outputTrace(const std::vector<uint8_t> &data)
{
  outputTrace(toString(data));
}

std::string Logger::toString(const std::vector<uint8_t>& value)
{
  std::stringstream stream;


  for (const auto itr : value) {
    stream << std::setfill('0') << std::setw(2) << std::hex << int(itr);
    stream << " ";
  }

  return stream.str();
}


}
