/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <QString>
#include <QDir>
#include <QIODevice>

namespace core::util
{


class Filesystem
{
public:
  virtual ~Filesystem() = default;

  virtual QIODevice* getFile(const QString&) = 0;

  virtual bool exists(const QString&) const = 0;
  virtual bool isDirectory(const QString&) const = 0;

  virtual QStringList listFiles(const QDir&) const = 0;
  virtual QStringList listDirectories(const QDir&) const = 0;
};


}
