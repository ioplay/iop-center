#include "SimulatedTimer.h"
#include <algorithm>
#include <cassert>

namespace core::util::simulated
{


Timer::Id SimulatedTimer::startCyclic(std::chrono::milliseconds timeout, const Handler &handler)
{
  return start(timeout, Type::Cyclic, handler);
}

Timer::Id SimulatedTimer::startSingle(std::chrono::milliseconds timeout, const Timer::Handler& handler)
{
  return start(timeout, Type::Single, handler);
}

Timer::Id SimulatedTimer::start(std::chrono::milliseconds timeout, SimulatedTimer::Type type, const Timer::Handler& handler)
{
  assert(timeout.count() > 0);

  const auto id = generateId();
  schedule(timeout, {id, type, handler, timeout});

  return id;
}

void SimulatedTimer::stop(Timer::Id id)
{
  for (auto itr = active.begin(); itr != active.end(); ++itr) {
    if (itr->active.id == id) {
      active.erase(itr);
      return;
    }
  }
}

void SimulatedTimer::fireNext()
{
  assert(!active.empty());

  const auto currentIdx = active.begin();
  const Active current = currentIdx->active;
  active.erase(currentIdx);

  if (current.type == Type::Cyclic) {
    // reschedule cyclic timer
    schedule(current.timeout, current);
  }

  // fire
  current.handler(current.id);
}

Timer::Id SimulatedTimer::generateId()
{
  const auto id = nextId;

  //TODO handle overflow
  //TODO make sure we don't reuse a id that is still in use for a cyclic timer
  nextId++;

  return id;
}

std::optional<SimulatedTimer::Duration> SimulatedTimer::nextDueTime() const
{
  if (active.empty()) {
    return {};
  } else {
    return active.begin()->remaining;
  }
}

void SimulatedTimer::schedule(Duration due, const SimulatedTimer::Active& action)
{
  active.push_back({due, action});
  std::stable_sort(active.begin(), active.end(), [](const Running& left, const Running& right){
    return left.remaining < right.remaining;
  });
}

void SimulatedTimer::forwardTimeBy(Duration delta)
{
  for (auto& tmr : active) {
    tmr.remaining -= delta;
  }

  while (true) {
    const auto due = nextDueTime();
    if (!due || (due->count() > 0)) {
      return;
    }
    fireNext();
  }
}


}
