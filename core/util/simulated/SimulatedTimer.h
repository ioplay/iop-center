#pragma once

#include "core/util/Timer.h"
#include <list>
#include <optional>

namespace core::util::simulated
{


class SimulatedTimer :
    public Timer
{
public:
  using Duration = std::chrono::high_resolution_clock::duration;

  Id startCyclic(std::chrono::milliseconds, const Handler&) override;
  Id startSingle(std::chrono::milliseconds, const Handler&) override;
  void stop(Id) override;

  void forwardTimeBy(Duration);

private:
  Id nextId{1};

  enum class Type
  {
    Single,
    Cyclic,
  };
  struct Active
  {
    Id id;
    Type type;
    Handler handler;
    std::chrono::milliseconds timeout;
  };
  struct Running
  {
    Duration remaining;
    Active active;
  };
  std::vector<Running> active{};

  std::optional<Duration> nextDueTime() const;
  void schedule(Duration, const Active&);

  void fireNext();

  Id start(std::chrono::milliseconds, Type, const Handler&);
  Id generateId();

};


}
