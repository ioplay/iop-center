/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "../Filesystem.h"
#include <QMap>
#include <QByteArray>

namespace core::util::simulated
{


class Filesystem :
    public core::util::Filesystem
{
public:
  QIODevice* getFile(const QString&) override;
  bool exists(const QString&) const override;
  bool isDirectory(const QString&) const override;
  QStringList listFiles(const QDir&) const override;
  QStringList listDirectories(const QDir&) const override;

  void add(const QString& path, const QString& content);

private:
  class File
  {
  public:
    QByteArray content{};
  };

  class Directory
  {
  public:
    QMap<QString, File*> files{};
    QMap<QString, Directory*> directories{};

    File* addFile(const QString&);
    Directory* mkdir(const QString&);
  };

  Directory root{};

  const Directory* findDirectory(const QDir&) const;
  const File* findFile(const QString&) const;
  File* findFile(const QString&);
  Directory* mkdir(const QString&);
};


}
