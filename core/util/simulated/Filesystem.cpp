/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Filesystem.h"
#include <QBuffer>
#include <QDir>
#include <QFileInfo>
#include <QTextStream>

namespace core::util::simulated
{
namespace
{


QStringList splitPath(const QString& value)
{
  const auto normalized = QDir::cleanPath(value);
  const auto splitted = normalized.split("/", QString::SplitBehavior::SkipEmptyParts);
  return splitted;
}


}


QIODevice* Filesystem::getFile(const QString& path)
{
  const auto file = findFile(path);

  if (!file) {
    throw std::invalid_argument("file not found: " + path.toStdString());
  }

  return new QBuffer(&file->content);
}

bool Filesystem::exists(const QString& path) const
{
  const auto directory = findDirectory(path);
  if (directory) {
    return true;
  }

  const auto file = findFile(path);
  if (file) {
    return true;
  }

  return false;
}

bool Filesystem::isDirectory(const QString& path) const
{
  const auto node = findDirectory(path);
  const auto result = node != nullptr;
  return result;
}

QStringList Filesystem::listFiles(const QDir& path) const
{
  const auto node = findDirectory(path.path());
  return node ? node->files.keys() : QStringList{};
}

QStringList Filesystem::listDirectories(const QDir& path) const
{
  const auto node = findDirectory(path.path());
  return node ? node->directories.keys() : QStringList{};
}

void Filesystem::add(const QString& path, const QString& content)
{
  if (exists(path)) {
    throw std::runtime_error("file or directory already exists: " + path.toStdString());
  }

  const auto isFile = !path.endsWith("/");

  const QFileInfo fileInfo{path};
  const auto parent = isFile ? fileInfo.dir() : QDir{path};
  auto dir = mkdir(parent.path());

  if (isFile) {
    auto file = dir->addFile(fileInfo.fileName());
    QBuffer buffer{&file->content};
    buffer.open(QIODevice::WriteOnly);
    QTextStream stream{&file->content};
    stream << content;
  }
}

const Filesystem::Directory* Filesystem::findDirectory(const QDir& dir) const
{
  const Directory* current = &root;

  const auto parts = splitPath(dir.path());
  for (const auto& part : parts) {
    current = current->directories.value(part, {});
    if (!current) {
      return nullptr;
    }
  }

  return current;
}

const Filesystem::File* Filesystem::findFile(const QString& path) const
{
  const QFileInfo fileInfo{path};
  const auto dirPath = fileInfo.dir();
  auto dir = findDirectory(dirPath);

  if (!dir) {
    return {};
  }

  const auto fileName = fileInfo.fileName();
  return dir->files.value(fileName, {});
}

Filesystem::File* Filesystem::findFile(const QString& path)
{
  const File* node = const_cast<const Filesystem*>(this)->findFile(path);
  File* result = const_cast<File*>(node);
  return result;
}

Filesystem::Directory* Filesystem::mkdir(const QString& path)
{
  Directory* result = &root;

  const auto parts = splitPath(path);
  for (const auto& part : parts) {
    result = result->mkdir(part);
  }

  return result;
}

Filesystem::File* Filesystem::Directory::addFile(const QString& name)
{
  if (name == "") {
    throw std::invalid_argument("file name can not be empty");
  }

  if (files.contains(name)) {
    throw std::runtime_error("file already exists: " + name.toStdString());
  }

  auto file = new File();
  files[name] = file;
  return file;
}

Filesystem::Directory* Filesystem::Directory::mkdir(const QString& name)
{
  if (name == "") {
    throw std::invalid_argument("directory name can not be empty");
  }

  if (directories.contains(name)) {
    return directories[name];
  }

  auto dir = new Directory();
  directories[name] = dir;
  return dir;
}


}

