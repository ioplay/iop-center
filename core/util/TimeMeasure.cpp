#include "TimeMeasure.h"

namespace core::util
{


void TimeMeasure::stop()
{
  stopTime = std::chrono::high_resolution_clock::now();
}

std::chrono::system_clock::duration TimeMeasure::delta() const
{
  return stopTime - startTime;
}

std::ostream &operator<<(std::ostream &stream, const TimeMeasure &measure)
{
  const auto delta = std::chrono::duration_cast<std::chrono::microseconds>(measure.delta());
  stream << delta.count() << "us";
  return stream;
}


}
