#pragma once

#include <functional>
#include <chrono>

namespace core::util
{


class Timer
{
public:
  virtual ~Timer() = default;

  using Id = int;
  using Handler = std::function<void(Id)>;

  virtual Id startCyclic(std::chrono::milliseconds, const Handler&) = 0;
  virtual Id startSingle(std::chrono::milliseconds, const Handler&) = 0;
  virtual void stop(Id) = 0;

};


}
