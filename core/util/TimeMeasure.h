#pragma once

#include <chrono>
#include <ostream>

namespace core::util
{


class TimeMeasure
{
public:
  void stop();

  std::chrono::high_resolution_clock::duration delta() const;

private:
  const std::chrono::high_resolution_clock::time_point startTime = std::chrono::high_resolution_clock::now();
  std::chrono::high_resolution_clock::time_point stopTime;

};

std::ostream& operator<<(std::ostream&, const TimeMeasure&);


}
