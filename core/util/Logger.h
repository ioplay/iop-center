/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <string>
#include <vector>

namespace core::util
{


class Logger
{
public:
  Logger(const std::string& module);

  void inputTrace(const std::string& data);
  void outputTrace(const std::string& data);

  void inputTrace(const std::vector<uint8_t>& data);
  void outputTrace(const std::vector<uint8_t>& data);

private:
  const std::string module;

  static std::string toString(const std::vector<uint8_t>&) ;
};


}
