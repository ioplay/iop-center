#pragma once

namespace core::base
{

class Component;


class ComponentFactory
{
public:
  virtual ~ComponentFactory() = default;

  virtual base::Component* produce() = 0;
};


}
