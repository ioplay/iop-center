#pragma once

#include "event/event.h"
#include <QObject>
#include <QMetaType>

Q_DECLARE_METATYPE(event::SharedEvent)
static const int SharedEventTypeNeededForRegistration = qRegisterMetaType<event::SharedEvent>();

namespace core::base
{


class Component :
    public QObject
{
  Q_OBJECT

public:
  virtual void initialize();
  virtual void start();
  virtual void stop();
  virtual void deinitialize();

  virtual void recvEvent(const event::SharedEvent&);

signals:
  void initializeComplete();
  void initializeError();
  void startComplete();
  void startError();
  void stopComplete();
  void stopError();
  void deinitializeComplete();
  void deinitializeError();

  void sendEvent(const event::SharedEvent&);
};


class EventAdapter : //TODO move to own file
    public event::Handler
{
public:
  EventAdapter(core::base::Component&);

  void handle(const event::SharedEvent&) override;

private:
  core::base::Component& component;

};


}

