#include "Component.h"

#include "event/event.h"
#include <QMetaType>
#include <QObject>

namespace core::base
{

void Component::initialize()
{
  initializeComplete();
}

void Component::start()
{
  startComplete();
}

void Component::stop()
{
  stopComplete();
}

void Component::deinitialize()
{
  deinitializeComplete();
}

void Component::recvEvent(const event::SharedEvent&)
{
}


EventAdapter::EventAdapter(Component& component_) :
  component{component_}
{
}

void EventAdapter::handle(const event::SharedEvent& event)
{
  component.sendEvent(event);
}


}

