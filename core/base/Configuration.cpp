/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Configuration.h"
#include "Component.h"
#include "core/util/Filesystem.h"
#include <QDebug>

namespace core::base
{


Configuration::Configuration(
    util::Filesystem& filesystem_,
    const QString& applicationName_
    ) :
  filesystem{filesystem_},
  applicationName{applicationName_}
{
}

void Configuration::load(const QString& filename, const Component& component, const Callback& callback) const
{
  const auto configFileName = directory(component) + filename;

  if (!filesystem.exists(configFileName)) {
    qCritical() << "file not found: " << configFileName;
    return;
  }

  const auto device = filesystem.getFile(configFileName);
  if (device->open(QIODevice::ReadOnly)) {
    QTextStream stream{device};
    parse(stream, callback);
  } else {
    qCritical() << "could not open " << configFileName;
  }
  delete device;
}

QString Configuration::directory(const Component& component) const
{
  const auto result{"/var/lib/" + applicationName + "/" + component.objectName() + "/"};
  return result;
}

void Configuration::parse(QTextStream& stream, const Callback& callback)
{
  while (!stream.atEnd()) {
    const auto line = stream.readLine();
    parseLine(line, callback);
  }
}

void Configuration::parseLine(const QString& line, const Configuration::Callback& callback)
{
  if (line.startsWith('#')) {
    return;
  }

  const QChar Separator = '=';
  const auto sepPos = line.indexOf(Separator);

  if (sepPos < 0) {
    return;
  }

  const auto key = line.mid(0, sepPos);
  const auto value = line.mid(sepPos+1, -1);

  callback(key, value);
}


}
