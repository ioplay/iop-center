/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "../Component.h"
#include "Transmitter.h"
#include <gtest/gtest.h>


namespace core::base::test
{
namespace
{


TEST(Component, can_use_SharedEvent_in_queued_connection)
{
  Transmitter transmitter{};

  const auto connection = QObject::connect(&transmitter, SIGNAL(send(const event::SharedEvent&)), &transmitter, SLOT(recv(const event::SharedEvent&)), Qt::QueuedConnection);

  ASSERT_TRUE(connection);
}


}
}
