/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "event/event.h"
#include <QObject>

namespace core::base::test
{


class Transmitter :
    public QObject
{
  Q_OBJECT

signals:
  void send(const event::SharedEvent&);

public slots:
  void recv(const event::SharedEvent&)
  {
  }

};


}
