/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "../Configuration.h"
#include <QBuffer>
#include <QMap>
#include <gtest/gtest.h>


namespace core::base::test
{
namespace
{


class Configuration_Test :
    public testing::Test
{
public:
  using Map = QMap<QString, QString>;

  static Map parse(const QString& data)
  {
    QBuffer buffer{};
    buffer.open(QIODevice::ReadWrite);
    QTextStream stream{&buffer};
    stream << data;
    stream.seek(0);

    Map result{};
    Configuration::parse(stream, [&result](const QString& key, const QString& value){
      result.insert(key, value);
    });
    return result;
  }

};


TEST_F(Configuration_Test, return_nothing_for_empty_file)
{
  const Map expected{};

  const auto actual = parse("");

  ASSERT_EQ(expected, actual);
}

TEST_F(Configuration_Test, parse_1_entry)
{
  const Map expected{
    {"test", "1234"},
  };

  const auto actual = parse("test=1234");

  ASSERT_EQ(expected, actual);
}

TEST_F(Configuration_Test, parse_2_entries)
{
  const Map expected{
    {"test", "1234"},
    {"hallo", "welt"},
  };

  const auto actual = parse("test=1234\nhallo=welt");

  ASSERT_EQ(expected, actual);
}

TEST_F(Configuration_Test, parse_3_entries)
{
  const Map expected{
    {"test", "1234"},
    {"hallo", "welt"},
    {"x", "y"},
  };

  const auto actual = parse("test=1234\nhallo=welt\nx=y");

  ASSERT_EQ(expected, actual);
}

TEST_F(Configuration_Test, parse_4_entries)
{
  const Map expected{
    {"test", "1234"},
    {"hallo", "welt"},
    {"x", "y"},
    {"12", "xy"},
  };

  const auto actual = parse("test=1234\nhallo=welt\nx=y\n12=xy");

  ASSERT_EQ(expected, actual);
}

TEST_F(Configuration_Test, parse_entries_with_empty_line_at_end)
{
  const Map expected{
    {"test", "1234"},
    {"hallo", "welt"},
  };

  const auto actual = parse("test=1234\nhallo=welt\n");

  ASSERT_EQ(expected, actual);
}

TEST_F(Configuration_Test, parse_entries_with_empty_line_in_between)
{
  const Map expected{
    {"test", "1234"},
    {"hallo", "welt"},
  };

  const auto actual = parse("test=1234\n\n\nhallo=welt");

  ASSERT_EQ(expected, actual);
}

TEST_F(Configuration_Test, ignore_lines_starting_with_a_hash)
{
  const Map expected{
    {"hallo", "welt"},
  };

  const auto actual = parse("#test=1234\nhallo=welt");

  ASSERT_EQ(expected, actual);
}


}
}
