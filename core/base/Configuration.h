/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <QString>
#include <QTextStream>
#include <functional>

namespace core::util
{

class Filesystem;

}

namespace core::base
{

class Component;


class Configuration
{
public:
  using Callback = std::function<void(const QString& key, const QString& value)>;

  Configuration(
      util::Filesystem&,
      const QString& applicationName
      );

  void load(const QString& filename, const Component&, const Callback&) const;
  QString directory(const Component&) const;

  static void parse(QTextStream&, const Callback&);

private:
  util::Filesystem& filesystem;
  const QString applicationName;

  static void parseLine(const QString&, const Configuration::Callback&);
};


}
