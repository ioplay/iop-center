/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include <QIODevice>
#include <QDomElement>
#include <QMap>

namespace core::setup
{

class SystemBuilder;


class XmlParser
{
public:
  XmlParser(SystemBuilder&);

  void parse(QIODevice*);

private:
  SystemBuilder& builder;

  void handleThread(const QDomElement&);
  void handleComponent(const QDomElement&);
  void handleConnection(const QDomElement&);
  void nullHandler(const QDomElement&);

  typedef void (XmlParser::*Handler)(const QDomElement&);

  const QMap<QString, Handler> Handlers{
    {"thread", &XmlParser::handleThread},
    {"component", &XmlParser::handleComponent},
    {"connection", &XmlParser::handleConnection},
  };

  void dispatch(const QDomElement& node);
};


}
