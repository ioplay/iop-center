/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Builder.h"
#include "FactoryRepository.h"
#include "core/base/Component.h"
#include "core/main/ComponentControl.h"
#include "core/thread/Thread.h"
#include "core/thread/ThreadedFactory.h"

namespace core::setup
{


Builder::Builder(
    main::ComponentControl& app_,
    FactoryRepository& componentFactories_,
    thread::Thread& threadFactory_
    ) :
  app{app_},
  componentFactories{componentFactories_},
  threadFactory{threadFactory_}
{
}

void Builder::buildThread(const QString& name)
{
  auto thread = threadFactory.produce();
  thread->setObjectName(name);
  threads[name] = thread;
}

void Builder::buildComponent(const QString& className, const QString& instanceName, const std::optional<QString>& threadName)
{
  const auto factory = componentFactories.find(className);
  if (!factory) {
    throw std::runtime_error("component class not found: " + className.toStdString());
  }

  const std::optional<QThread*> thread = threadName ? getThread(*threadName) : std::optional<QThread*>{};

  thread::ThreadedFactory tf{};
  auto component = tf.produce(*factory, thread);
  component->setObjectName(instanceName);
  app.connect(component);
}

void Builder::buildConnection(const QString& from, const QString& to)
{
  const auto fromComponent = app.findComponent(from);
  if (!fromComponent) {
    throw std::runtime_error("component instance not found: " + from.toStdString());
  }
  const auto toComponent = app.findComponent(to);
  if (!toComponent) {
    throw std::runtime_error("component instance not found: " + to.toStdString());
  }

  QObject::connect(fromComponent, &base::Component::sendEvent, toComponent, &base::Component::recvEvent);
}

QThread* Builder::getThread(const QString& name)
{
  const auto thread = threads.value(name, {});
  if (!thread) {
    throw std::runtime_error("thread not found: " + name.toStdString());
  }
  return thread;
}


}
