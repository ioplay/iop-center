/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "core/component/keyboard-switch/Factory.h"
#include "core/component/matchplay/Factory.h"
#include "core/component/mpd/Factory.h"
#include "core/component/printer/Factory.h"
#include "core/component/rfid/Factory.h"
#include "core/component/stage-notification/Factory.h"

namespace core::base
{

class Configuration;

}

namespace core::setup
{


struct Factories
{
  Factories(
      std::ostream& cout,
      component::rfid::RegisterDevice& mfrc522Registers,
      component::mpd::SocketFactory&,
      util::Filesystem&,
      util::Timer&,
      const base::Configuration&
      );

  component::keyboard_switch::Factory keyboardSwitch;
  component::matchplay::Factory matchplay;
  component::mpd::Factory mpd;
  component::printer::Factory printer;
  component::rfid::Factory rfid;
  component::stage_notification::Factory stageNotification;
};


}
