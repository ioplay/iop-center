/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "SystemBuilder.h"
#include <QString>
#include <QMap>
#include <QThread>

namespace core::main
{

class ComponentControl;

}

namespace core::thread
{

class Thread;

}

namespace core::setup
{

class FactoryRepository;


class Builder :
    public SystemBuilder
{
public:
  Builder(
      main::ComponentControl&,
      FactoryRepository&,
      thread::Thread&
      );

  void buildThread(const QString& name) override;
  void buildComponent(const QString& className, const QString& instanceName, const std::optional<QString>& threadName) override;
  void buildConnection(const QString& from, const QString& to) override;

private:
  main::ComponentControl& app;
  FactoryRepository& componentFactories;
  thread::Thread& threadFactory;
  QMap<QString, QThread*> threads{};

  QThread* getThread(const QString& name);

};


}
