/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <QString>
#include <optional>

namespace core::setup
{


class SystemBuilder
{
public:
  virtual ~SystemBuilder() = default;

  virtual void buildThread(const QString& name) = 0;
  virtual void buildComponent(const QString& className, const QString& instanceName, const std::optional<QString>& threadName) = 0;
  virtual void buildConnection(const QString& from, const QString& to) = 0;
};


}
