/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "registerFactories.h"
#include "Factories.h"
#include "FactoryRepository.h"

namespace core::setup
{


void registerFactories(FactoryRepository& repo, Factories& factories)
{
  repo.add("KeyboardSwitch", factories.keyboardSwitch);
  repo.add("Matchplay", factories.matchplay);
  repo.add("Mpd", factories.mpd);
  repo.add("Printer", factories.printer);
  repo.add("Rfid", factories.rfid);
  repo.add("StageNotification", factories.stageNotification);
}


}
