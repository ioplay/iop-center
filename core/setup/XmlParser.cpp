/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "XmlParser.h"
#include "SystemBuilder.h"

namespace core::setup
{


XmlParser::XmlParser(SystemBuilder& builder_) :
  builder{builder_}
{
}

void XmlParser::parse(QIODevice* content)
{
  QDomDocument doc{};
  doc.setContent(content);
  for (auto element = doc.documentElement().firstChildElement(); !element.isNull(); element = element.nextSiblingElement()) {
    dispatch(element);
  }
}

void XmlParser::handleThread(const QDomElement& element)
{
  const auto id = element.attribute("id");
  builder.buildThread(id);
}

void XmlParser::handleComponent(const QDomElement& element)
{
  const auto className = element.attribute("class");
  const auto instanceName = element.attribute("id");
  const auto threadElement = element.attributes().namedItem("thread");
  const auto threadName = threadElement.isNull() ? std::optional<QString>{} : threadElement.nodeValue();
  builder.buildComponent(className, instanceName, threadName);
}

void XmlParser::handleConnection(const QDomElement& element)
{
  const auto fromName = element.attribute("from");
  const auto toName = element.attribute("to");
  builder.buildConnection(fromName, toName);
}

void XmlParser::nullHandler(const QDomElement&)
{
}

void XmlParser::dispatch(const QDomElement& node)
{
  const auto name = node.nodeName();
  const auto handler = Handlers.value(name, &XmlParser::nullHandler);
  (this->*handler)(node);
}


}
