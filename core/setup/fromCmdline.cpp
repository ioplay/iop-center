/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "fromCmdline.h"
#include "Builder.h"
#include "XmlParser.h"
#include "core/util/Filesystem.h"
#include <QCommandLineParser>
#include <QDebug>


namespace core::setup
{
namespace
{


QString getApplicationName(const QStringList& arguments)
{
  const auto& appPath = arguments.first();
  const QFileInfo info{appPath};
  const auto appName = info.fileName();
  return appName;
}


}


QString getSetupFile(const QString& appName, const QString& cmdlineSetupFile, const util::Filesystem& fs)
{
  if (!cmdlineSetupFile.isEmpty()) {
    return cmdlineSetupFile;
  }

  if (!appName.isEmpty()) {
    QStringList testedFiles{};

    for (const auto prefix : {"/usr/local/lib/", "/usr/lib/"}) {
      const auto setup = prefix + appName + "/setup.xml";
      if (fs.exists(setup)) {
        return setup;
      }
      testedFiles.append(setup);
    }

    qDebug() << "no implicit setup file found, tested:" << testedFiles;
  }

  return {};
}

Arguments parseCmdline(const QStringList& arguments, const util::Filesystem& filesystem)
{
  QCommandLineParser cmdParser{};
  cmdParser.addHelpOption();

  QCommandLineOption setupOption{"setup", "file with the system setup", "file"};
  cmdParser.addOption(setupOption);

  QCommandLineOption nameOption{"name", "name to use for the application", "name"};
  cmdParser.addOption(nameOption);

  cmdParser.process(arguments);

  Arguments result{};

  result.applicationName = cmdParser.isSet(nameOption) ? cmdParser.value(nameOption) : getApplicationName(arguments);
  result.setupFilename = getSetupFile(result.applicationName, cmdParser.value(setupOption), filesystem);

  return result;
}

void fromArguments(const Arguments& arguments, Builder& builder, util::Filesystem& filesystem)
{
  if (!arguments.setupFilename.isEmpty()) {
    qDebug() << "using setup file:" << arguments.setupFilename;
    auto file = filesystem.getFile(arguments.setupFilename);
    core::setup::XmlParser parser{builder};
    parser.parse(file);
  } else {
    qDebug() << "no setup file found";
  }
}


}
