/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <QStringList>

namespace core::util
{

class Filesystem;

}

namespace core::setup
{

class Builder;


struct Arguments
{
  QString applicationName{};
  QString setupFilename{};
};

QString getSetupFile(const QString& appName, const QString& cmdlineSetupFile, const util::Filesystem&);
Arguments parseCmdline(const QStringList& arguments, const util::Filesystem&);
void fromArguments(const Arguments&, Builder&, util::Filesystem&);


}
