/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "FactoryRepository.h"

namespace core::setup
{


base::ComponentFactory* FactoryRepository::find(const QString& name)
{
  return factories.value(name, {});
}

void FactoryRepository::add(const QString& name, base::ComponentFactory& factory)
{
  if (factories.contains(name)) {
    throw std::runtime_error("factory already registered: " + name.toStdString());
  }
  factories.insert(name, &factory);
}


}
