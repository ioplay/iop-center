/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "Factories.h"

namespace core::setup
{


Factories::Factories(
    std::ostream& cout,
    component::rfid::RegisterDevice& mfrc522Registers,
    component::mpd::SocketFactory& socketFactory,
    util::Filesystem& filesystem,
    util::Timer& timer,
    const base::Configuration& configuration
    ) :
  keyboardSwitch{},
  matchplay{cout, configuration, filesystem},
  mpd{socketFactory},
  printer{cout},
  rfid{mfrc522Registers, timer},
  stageNotification{}
{
}


}
