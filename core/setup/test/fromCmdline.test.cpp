/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "core/setup/fromCmdline.h"
#include "core/util/simulated/Filesystem.h"
#include <gtest/gtest.h>


namespace core::setup::test
{
namespace
{


class setup_fromCmdline_Test :
    public testing::Test
{
public:
  util::simulated::Filesystem fs{};

};


TEST_F(setup_fromCmdline_Test, return_setup_from_command_line_when_specified)
{
  fs.add("/usr/lib/test-app/setup.xml", {});

  const auto actual = getSetupFile("test-app", "test/hello.xml", fs);

  ASSERT_EQ("test/hello.xml", actual.toStdString());
}

TEST_F(setup_fromCmdline_Test, return_setup_from_lib_when_no_command_line_argument_is_specified)
{
  fs.add("/usr/lib/test-app/setup.xml", {});

  const auto actual = getSetupFile("test-app", {}, fs);

  ASSERT_EQ("/usr/lib/test-app/setup.xml", actual.toStdString());
}

TEST_F(setup_fromCmdline_Test, return_setup_from_local_lib_when_available)
{
  fs.add("/usr/lib/test-app/setup.xml", {});
  fs.add("/usr/local/lib/test-app/setup.xml", {});

  const auto actual = getSetupFile("test-app", {}, fs);

  ASSERT_EQ("/usr/local/lib/test-app/setup.xml", actual.toStdString());
}

TEST_F(setup_fromCmdline_Test, returns_nothing_when_no_setup_file_is_available)
{

  const auto actual = getSetupFile("test-app", {}, fs);

  ASSERT_EQ("", actual.toStdString());
}


}
}
