/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

namespace core::setup
{

struct Factories;
class FactoryRepository;


void registerFactories(FactoryRepository&, Factories&);


}
