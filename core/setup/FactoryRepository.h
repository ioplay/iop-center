/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <QString>
#include <QMap>

namespace core::base
{

class ComponentFactory;

}

namespace core::setup
{


class FactoryRepository
{
public:
  base::ComponentFactory* find(const QString& name);
  void add(const QString& name, base::ComponentFactory&);

private:
  QMap<QString, base::ComponentFactory*> factories{};

};


}
