/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "LogComponent.h"


namespace core::main::test
{


LogComponent::LogComponent(const std::string& name_, std::vector<std::string>& log_) :
  name{name_},
  log{log_}
{
}

void LogComponent::initialize()
{
  log.push_back(name + "." + __func__);
}

void LogComponent::start()
{
  log.push_back(name + "." + __func__);
}

void LogComponent::stop()
{
  log.push_back(name + "." + __func__);
}

void LogComponent::deinitialize()
{
  log.push_back(name + "." + __func__);
}


}
