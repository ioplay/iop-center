/*
 * (C) Copyright 2019 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "../ComponentControl.h"
#include "LogComponent.h"
#include <QCoreApplication>
#include <gtest/gtest.h>


namespace core::main::test
{
namespace
{

using namespace testing;

using Log = std::vector<std::string>;


class ComponentControlTest :
    public Test
{
public:
  int argc{1};
  char* argv{"-test-"};
  QCoreApplication app{argc, &argv};

  Log log{};

  LogComponent compA{"A", log};
  LogComponent compB{"B", log};
  LogComponent compC{"C", log};

  ComponentControl testee{};

  void SetUp() override
  {
    testee.connect(&compA);
    testee.connect(&compB);
    testee.connect(&compC);
  }

};


TEST_F(ComponentControlTest, initializes_components_in_order_when_booting)
{
  const Log expected{
    "A.initialize",
    "B.initialize",
    "C.initialize",
  };

  testee.boot();

  ASSERT_EQ(expected, log);
}

TEST_F(ComponentControlTest, start_components_in_order_when_all_are_initialized)
{
  const Log expected{
    "A.start",
    "B.start",
    "C.start",
  };
  testee.boot();
  log.clear();

  compB.initializeComplete();
  compA.initializeComplete();
  compC.initializeComplete();
  QCoreApplication::processEvents();

  ASSERT_EQ(expected, log);
}

TEST_F(ComponentControlTest, notify_when_system_is_booted)
{
  bool isBooted = false;
  QObject::connect(&testee, &ComponentControl::bootComplete, [&isBooted]{
    isBooted = true;
  });

  testee.boot();
  compB.initializeComplete();
  compA.initializeComplete();
  compC.initializeComplete();
  QCoreApplication::processEvents();
  compB.startComplete();
  compC.startComplete();
  compA.startComplete();
  QCoreApplication::processEvents();

  ASSERT_TRUE(isBooted);
}

TEST_F(ComponentControlTest, notify_when_system_is_booted_with_no_components)
{
  ComponentControl testee{};
  bool isBooted = false;
  QObject::connect(&testee, &ComponentControl::bootComplete, [&isBooted]{
    isBooted = true;
  });

  testee.boot();

  ASSERT_TRUE(isBooted);
}

TEST_F(ComponentControlTest, stops_components_in_order_when_shutting_down)
{
  const Log expected{
    "C.stop",
    "B.stop",
    "A.stop",
  };

  testee.shutdown();

  ASSERT_EQ(expected, log);
}

TEST_F(ComponentControlTest, deinitialize_components_in_order_when_all_stopped)
{
  const Log expected{
    "C.deinitialize",
    "B.deinitialize",
    "A.deinitialize",
  };
  testee.shutdown();
  log.clear();

  compC.stopComplete();
  compA.stopComplete();
  compB.stopComplete();
  QCoreApplication::processEvents();

  ASSERT_EQ(expected, log);
}

TEST_F(ComponentControlTest, notify_when_system_is_shutdown)
{
  bool isShutdown = false;
  QObject::connect(&testee, &ComponentControl::shutdownComplete, [&isShutdown]{
    isShutdown = true;
  });

  testee.shutdown();
  compB.stopComplete();
  compA.stopComplete();
  compC.stopComplete();
  QCoreApplication::processEvents();
  compB.deinitializeComplete();
  compC.deinitializeComplete();
  compA.deinitializeComplete();
  QCoreApplication::processEvents();

  ASSERT_TRUE(isShutdown);
}

TEST_F(ComponentControlTest, notify_when_system_is_shutdown_with_no_components)
{
  ComponentControl testee{};
  bool isShutdown = false;
  QObject::connect(&testee, &ComponentControl::shutdownComplete, [&isShutdown]{
    isShutdown = true;
  });

  testee.shutdown();

  ASSERT_TRUE(isShutdown);
}

TEST_F(ComponentControlTest, sane_behaviour_when_components_send_spurious_initialize_complete_signals_during_shutdown)
{
  const Log expected{
  };
  testee.shutdown();
  log.clear();
  compA.stopComplete();
  compC.stopComplete();
  QCoreApplication::processEvents();

  compB.initializeComplete();
  QCoreApplication::processEvents();

  ASSERT_EQ(expected, log);
}

TEST_F(ComponentControlTest, sane_behaviour_when_components_send_spurious_stop_complete_signals_during_initialization)
{
  const Log expected{
  };
  testee.boot();
  log.clear();
  compB.initializeComplete();
  compC.initializeComplete();
  QCoreApplication::processEvents();

  compA.stopComplete();
  QCoreApplication::processEvents();

  ASSERT_EQ(expected, log);
}

TEST_F(ComponentControlTest, waits_for_all_components_when_an_error_occured_during_initialization)
{
  const Log expected{
  };
  testee.boot();
  log.clear();

  compA.initializeComplete();
  compB.initializeError();
  QCoreApplication::processEvents();

  ASSERT_EQ(expected, log);
}

TEST_F(ComponentControlTest, deinitialize_other_components_when_an_error_occured_during_initialization)
{
  const Log expected{
    "C.deinitialize",
    "A.deinitialize",
  };
  testee.boot();
  log.clear();

  compA.initializeComplete();
  compB.initializeError();
  compC.initializeComplete();
  QCoreApplication::processEvents();

  ASSERT_EQ(expected, log);
}

TEST_F(ComponentControlTest, is_shutdown_when_non_error_components_are_deinitialized)
{
  bool isShutdown = false;
  QObject::connect(&testee, &ComponentControl::shutdownComplete, [&isShutdown]{
    isShutdown = true;
  });
  testee.boot();
  compA.initializeComplete();
  compB.initializeError();
  compC.initializeComplete();
  QCoreApplication::processEvents();

  compA.deinitializeComplete();
  compC.deinitializeComplete();
  QCoreApplication::processEvents();

  ASSERT_TRUE(isShutdown);
}

TEST_F(ComponentControlTest, stops_components_when_an_error_occured_during_start)
{
  const Log expected{
    "C.stop",
  };
  testee.boot();
  compB.initializeComplete();
  compA.initializeComplete();
  compC.initializeComplete();
  QCoreApplication::processEvents();
  log.clear();

  compA.startError();
  compB.startError();
  compC.startComplete();
  QCoreApplication::processEvents();

  ASSERT_EQ(expected, log);
}

TEST_F(ComponentControlTest, deinitialize_components_when_an_error_occured_during_start)
{
  const Log expected{
    "C.deinitialize",
    "B.deinitialize",
    "A.deinitialize",
  };
  testee.boot();
  compB.initializeComplete();
  compA.initializeComplete();
  compC.initializeComplete();
  QCoreApplication::processEvents();
  compA.startError();
  compB.startError();
  compC.startComplete();
  QCoreApplication::processEvents();
  log.clear();

  compC.stopComplete();
  QCoreApplication::processEvents();

  ASSERT_EQ(expected, log);
}

TEST_F(ComponentControlTest, deinitialize_all_components_when_one_component_had_an_error_during_stop)
{
  const Log expected{
    "C.deinitialize",
    "B.deinitialize",
    "A.deinitialize",
  };
  testee.shutdown();
  log.clear();

  compA.stopComplete();
  compB.stopError();
  compC.stopComplete();
  QCoreApplication::processEvents();

  ASSERT_EQ(expected, log);
}

TEST_F(ComponentControlTest, is_shutdown_when_some_components_had_errors)
{
  bool isShutdown = false;
  QObject::connect(&testee, &ComponentControl::shutdownComplete, [&isShutdown]{
    isShutdown = true;
  });
  testee.shutdown();
  compA.stopComplete();
  compB.stopComplete();
  compC.stopComplete();
  QCoreApplication::processEvents();

  compA.deinitializeComplete();
  compB.deinitializeError();
  compC.deinitializeComplete();
  QCoreApplication::processEvents();

  ASSERT_TRUE(isShutdown);
}

TEST_F(ComponentControlTest, does_not_synchroniously_start_next_stage)
{
  const Log expected{
  };
  testee.boot();
  log.clear();

  compB.initializeComplete();
  compA.initializeComplete();
  compC.initializeComplete();

  ASSERT_EQ(expected, log);
}


}
}
