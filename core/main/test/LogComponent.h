/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include "core/base/Component.h"

namespace core::main::test
{


class LogComponent :
    public base::Component
{
public:
  LogComponent(const std::string& name_, std::vector<std::string>& log_);

  void initialize() override;
  void start() override;
  void stop() override;
  void deinitialize() override;

private:
  const std::string name;
  std::vector<std::string>& log;

};


}
