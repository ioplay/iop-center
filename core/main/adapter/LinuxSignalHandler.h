#pragma once

#include <QSocketNotifier>
#include <QObject>

namespace core::main::adapter
{


class LinuxSignalHandler :
    public QObject
{
  Q_OBJECT

public:
  LinuxSignalHandler();
  ~LinuxSignalHandler();

signals:
  void shutdown();

private:
  int sfd{-1};
  QSocketNotifier* notifier;

  void socketActivated();
};


}
