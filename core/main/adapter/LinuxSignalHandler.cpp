#include "LinuxSignalHandler.h"

#include <csignal>
#include <stdexcept>
#include <sys/signalfd.h>
#include <unistd.h>
#include <QDebug>

namespace core::main::adapter
{


LinuxSignalHandler::LinuxSignalHandler()
{
  sigset_t mask;

  sigemptyset(&mask);
  sigaddset(&mask, SIGINT);
  sigaddset(&mask, SIGQUIT);
  sigaddset(&mask, SIGTERM);

  if (sigprocmask(SIG_BLOCK, &mask, nullptr) == -1) {
    throw std::runtime_error("could not sigprocmask: " + std::to_string(errno));
  }

  sfd = signalfd(-1, &mask, SFD_NONBLOCK);
  if (sfd == -1) {
    throw std::runtime_error("could not signalfd: " + std::to_string(errno));
  }

  notifier = new QSocketNotifier(sfd, QSocketNotifier::Read, this);
  connect(notifier, &QSocketNotifier::activated, this, &LinuxSignalHandler::socketActivated);
}

void LinuxSignalHandler::socketActivated()
{
  while (true) {
    struct signalfd_siginfo fdsi;
    ssize_t s;

    s = read(sfd, &fdsi, sizeof(struct signalfd_siginfo));
    if (s != sizeof(struct signalfd_siginfo)) {
      shutdown();
      return;
    }

    qDebug() << "got signal" << fdsi.ssi_signo;
  }
}

LinuxSignalHandler::~LinuxSignalHandler()
{
  close(sfd);
}


}
