#pragma once

#include <QObject>
#include <QList>
#include <QSet>
#include <QMap>

namespace core::base
{

class Component;

}

namespace core::main
{


class ComponentControl :
    public QObject
{
  Q_OBJECT

public:
  void boot();
  void shutdown();

  void connect(base::Component*);

  base::Component* findComponent(const QString& instanceName);

signals:
  void bootComplete();
  void shutdownComplete();

private slots:
  void initializeComplete();
  void initializeError();
  void startComplete();
  void startError();
  void stopComplete();
  void stopError();
  void deinitializeComplete();
  void deinitializeError();

private:
  QList<base::Component*> components{};
  QList<base::Component*> waitForAnswer{};
  enum class State
  {
    Uninitialized,
    Initializing,
    Starting,
    Stopping,
    Deinitializing,
  };
  State state{State::Uninitialized};
  QSet<base::Component*> errors{};

  void initialize();
  void start();
  void bootFinished();

  void stop();
  void deinitialize();
  void shutdownFinished();

  void componentErrorStage(QObject* sender, State expectedState);
  void componentCompletedStage(QObject* sender, State expectedState);

  typedef void (base::Component::*Method)();
  enum class Direction
  {
    Forward,
    Reverse,
  };
  void startStage(const Method& method, Direction direction);

  static void for_each(const QList<base::Component*>&, const Method&, Direction);
  static void invokeThreadSave(const Method&, base::Component*);

  typedef void (ComponentControl::*StageStarter)();
  static const QMap<State, StageStarter> NextStarterSuccess;
  static const QMap<State, StageStarter> NextStarterError;
};


}
