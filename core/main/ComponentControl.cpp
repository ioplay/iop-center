#include "ComponentControl.h"
#include "core/base/Component.h"
#include <QtGlobal>
#include <QDebug>
#include <algorithm>

namespace core::main
{


const QMap<ComponentControl::State, ComponentControl::StageStarter> ComponentControl::NextStarterSuccess
{
  {ComponentControl::State::Initializing, &ComponentControl::start},
  {ComponentControl::State::Starting, &ComponentControl::bootFinished},
  {ComponentControl::State::Stopping, &ComponentControl::deinitialize},
  {ComponentControl::State::Deinitializing, &ComponentControl::shutdownFinished},
};

const QMap<ComponentControl::State, ComponentControl::StageStarter> ComponentControl::NextStarterError
{
  {ComponentControl::State::Initializing, &ComponentControl::deinitialize},
  {ComponentControl::State::Starting, &ComponentControl::stop},
  {ComponentControl::State::Stopping, &ComponentControl::deinitialize},
  {ComponentControl::State::Deinitializing, &ComponentControl::shutdownFinished},
};


void ComponentControl::boot()
{
  if (components.empty()) {
    bootComplete();
  } else {
    initialize();
  }
}

void ComponentControl::initialize()
{
  state = State::Initializing;
  startStage(&base::Component::initialize, Direction::Forward);
}

void ComponentControl::initializeComplete()
{
  componentCompletedStage(sender(), State::Initializing);
}

void ComponentControl::initializeError()
{
  componentErrorStage(sender(), State::Initializing);
}

void ComponentControl::start()
{
  state = State::Starting;
  startStage(&base::Component::start, Direction::Forward);
}

void ComponentControl::startComplete()
{
  componentCompletedStage(sender(), State::Starting);
}

void ComponentControl::startError()
{
  componentErrorStage(sender(), State::Starting);
}

void ComponentControl::bootFinished()
{
  bootComplete();
}


void ComponentControl::shutdown()
{
  if (components.empty()) {
    shutdownComplete();
  } else {
    stop();
  }
}

void ComponentControl::stop()
{
  state = State::Stopping;
  startStage(&base::Component::stop, Direction::Reverse);
}

void ComponentControl::stopComplete()
{
  componentCompletedStage(sender(), State::Stopping);
}

void ComponentControl::stopError()
{
  componentCompletedStage(sender(), State::Stopping);
}

void ComponentControl::deinitialize()
{
  state = State::Deinitializing;
  startStage(&base::Component::deinitialize, Direction::Reverse);
}

void ComponentControl::deinitializeComplete()
{
  componentCompletedStage(sender(), State::Deinitializing);
}

void ComponentControl::deinitializeError()
{
  componentCompletedStage(sender(), State::Deinitializing);
}

void ComponentControl::shutdownFinished()
{
  shutdownComplete();
}

void ComponentControl::componentErrorStage(QObject* sender, ComponentControl::State expectedState)
{
  auto const component = dynamic_cast<base::Component*>(sender);
  Q_ASSERT(component);
  errors.insert(component);
  componentCompletedStage(sender, expectedState);
}

void ComponentControl::componentCompletedStage(QObject* sender, State expectedState)
{
  if (state == expectedState) {
    auto const component = dynamic_cast<base::Component*>(sender);
    Q_ASSERT(component);
    Q_ASSERT(waitForAnswer.contains(component));
    waitForAnswer.removeAll(component);

    if (waitForAnswer.empty()) {
      const auto& NextStarter = errors.empty() ? NextStarterSuccess : NextStarterError;
      const auto next = NextStarter.value(state, StageStarter{});
      if (next) {
        (*this.*next)();
      } else {
        qCritical() << "no starter for state" << static_cast<int>(state);
      }
    }
  }
}

void ComponentControl::connect(base::Component* comp)
{
  components.push_back(comp);
  QObject::connect(comp, &base::Component::initializeComplete, this, &ComponentControl::initializeComplete, Qt::ConnectionType::QueuedConnection);
  QObject::connect(comp, &base::Component::initializeError, this, &ComponentControl::initializeError, Qt::ConnectionType::QueuedConnection);
  QObject::connect(comp, &base::Component::startComplete, this, &ComponentControl::startComplete, Qt::ConnectionType::QueuedConnection);
  QObject::connect(comp, &base::Component::startError, this, &ComponentControl::startError, Qt::ConnectionType::QueuedConnection);
  QObject::connect(comp, &base::Component::stopComplete, this, &ComponentControl::stopComplete, Qt::ConnectionType::QueuedConnection);
  QObject::connect(comp, &base::Component::stopError, this, &ComponentControl::stopError, Qt::ConnectionType::QueuedConnection);
  QObject::connect(comp, &base::Component::deinitializeComplete, this, &ComponentControl::deinitializeComplete, Qt::ConnectionType::QueuedConnection);
  QObject::connect(comp, &base::Component::deinitializeError, this, &ComponentControl::deinitializeError, Qt::ConnectionType::QueuedConnection);
}

base::Component* ComponentControl::findComponent(const QString& instanceName)
{
  for (const auto component : components) {
    if (component->objectName() == instanceName) {
      return component;
    }
  }

  return {};
}

void ComponentControl::startStage(const Method& method, Direction direction)
{
  Q_ASSERT(waitForAnswer.empty());
  waitForAnswer = components;
  for (const auto errComp : errors) {
    waitForAnswer.removeAll(errComp);
  }
  errors.clear();
  for_each(waitForAnswer, method, direction);
}

void ComponentControl::for_each(const QList<base::Component*>& components, const Method& method, ComponentControl::Direction direction)
{
  const auto function = std::bind(invokeThreadSave, method, std::placeholders::_1);

  switch (direction) {
    case Direction::Forward:
      std::for_each(components.begin(), components.end(), function);
      break;

    case Direction::Reverse:
      std::for_each(components.rbegin(), components.rend(), function);
      break;
  }
}

void ComponentControl::invokeThreadSave(const Method& method, base::Component* component)
{
  QMetaObject::invokeMethod(component, method);
}


}
