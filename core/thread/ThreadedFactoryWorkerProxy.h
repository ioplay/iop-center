/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <QObject>

namespace core::base
{

class Component;
class ComponentFactory;

}

namespace core::thread
{


class ThreadedFactoryWorkerProxy :
    public QObject
{
  Q_OBJECT

signals:
  void workerProduce(base::ComponentFactory*);

public slots:
  void workerCreated(base::Component*);

public:
  void produce(base::ComponentFactory* factory);

  bool finished() const;
  base::Component* component() const;

private:
  base::Component* result{};
};


}
