/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "../ThreadedFactory.h"
#include "core/base/Component.h"
#include "core/base/ComponentFactory.h"
#include <QCoreApplication>
#include <QThread>
#include <gtest/gtest.h>


namespace core::thread::test
{
namespace
{


class TestComponent :
    public base::Component
{
public:
  QObject member{};
};

class TestFactory :
    public base::ComponentFactory
{
public:
  base::Component* produce() override
  {
    component = new TestComponent();
    return component;
  }

  TestComponent* component{};

};


using namespace testing;


class ThreadedFactoryTest :
    public Test
{
public:
  int argc{0};
  char* argv{};
  QCoreApplication app{argc, &argv};

  ThreadedFactory testee{};

  TestFactory factory{};
  QThread thread{};

  void SetUp() override
  {
    thread.start();
  }

  void TearDown() override
  {
    thread.quit();
    thread.wait();
  }

};


TEST_F(ThreadedFactoryTest, can_create_component_in_main_thread)
{

  auto component = testee.produce(factory, {});

  ASSERT_EQ(app.thread(), component->thread());
}

TEST_F(ThreadedFactoryTest, return_component_from_factory)
{

  auto component = testee.produce(factory, &thread);

  ASSERT_EQ(factory.component, component);
}

TEST_F(ThreadedFactoryTest, created_component_in_specified_thread)
{

  auto component = testee.produce(factory, &thread);

  ASSERT_EQ(&thread, component->thread());
}

TEST_F(ThreadedFactoryTest, created_members_of_component_in_specified_thread)
{

  testee.produce(factory, &thread);

  ASSERT_EQ(&thread, factory.component->member.thread());
}


}
}
