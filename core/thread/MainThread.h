#pragma once

#include "Thread.h"

namespace core::thread
{


class MainThread :
    public Thread
{
public:
  QThread* produce() override;

};


}
