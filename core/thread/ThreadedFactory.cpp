/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ThreadedFactory.h"
#include "ThreadedFactoryWorker.h"
#include "ThreadedFactoryWorkerProxy.h"
#include "core/base/Component.h"
#include "core/base/ComponentFactory.h"
#include <QAbstractEventDispatcher>
#include <QDebug>
#include <QThread>

namespace core::thread
{


base::Component* ThreadedFactory::produceNonThreaded(base::ComponentFactory& factory)
{
  return factory.produce();
}

base::Component* ThreadedFactory::produceThreaded(base::ComponentFactory& factory, QThread* thread)
{
  auto dispatcher = QThread::currentThread()->eventDispatcher();
  if (!dispatcher) {
    qFatal("no event dispatcher in current thread");
  }

  if (!thread->isRunning()) {
    qFatal("thread has to be started");
  }

  ThreadedFactoryWorker worker{};
  ThreadedFactoryWorkerProxy proxy{};
  worker.moveToThread(thread);

  QObject::connect(&proxy, &ThreadedFactoryWorkerProxy::workerProduce, &worker, &ThreadedFactoryWorker::produce);
  QObject::connect(&worker, &ThreadedFactoryWorker::created, &proxy, &ThreadedFactoryWorkerProxy::workerCreated);

  proxy.produce(&factory);

  while (!proxy.finished()) {
    QThread::yieldCurrentThread();
    dispatcher->processEvents(QEventLoop::AllEvents);
  }

  return proxy.component();
}

base::Component* ThreadedFactory::produce(base::ComponentFactory& factory, const std::optional<QThread*>& thread)
{
  if (thread) {
    return produceThreaded(factory, *thread);
  } else {
    return produceNonThreaded(factory);
  }
}


}
