/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ThreadedFactoryWorkerProxy.h"

namespace core::thread
{


void ThreadedFactoryWorkerProxy::workerCreated(base::Component* value)
{
  result = value;
}

void ThreadedFactoryWorkerProxy::produce(base::ComponentFactory* factory)
{
  result = {};
  workerProduce(factory);
}

bool ThreadedFactoryWorkerProxy::finished() const
{
  return result;
}

base::Component* ThreadedFactoryWorkerProxy::component() const
{
  return result;
}


}
