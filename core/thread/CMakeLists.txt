cmake_minimum_required (VERSION 3.8)

set(CMAKE_AUTOMOC ON)
find_package(Qt5 COMPONENTS Core REQUIRED)

add_library(core_thread STATIC
    MainThread.cpp
    MainThread.h
    Thread.h
    ThreadedFactory.cpp
    ThreadedFactory.h
    ThreadedFactoryWorker.cpp
    ThreadedFactoryWorker.h
    ThreadedFactoryWorkerProxy.cpp
    ThreadedFactoryWorkerProxy.h
)

include_directories(core_thread
    ${PROJECT_SOURCE_DIR}
)

target_link_libraries(core_thread
    core_base
)

target_link_libraries(core_thread
    Qt5::Core
)

set_target_properties(core_thread PROPERTIES
    CXX_STANDARD 17
)

target_compile_options(core_thread PRIVATE
    -Wall
    -Wextra
    -pedantic
)

