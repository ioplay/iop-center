#pragma once

#include <QThread>

namespace core::thread
{


class Thread
{
public:
  virtual ~Thread() = default;

  virtual QThread* produce() = 0;

};


}
