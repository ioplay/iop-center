#include "MainThread.h"
#include <QCoreApplication>

namespace core::thread
{


QThread* MainThread::produce()
{
  return QCoreApplication::instance()->thread();
}


}
