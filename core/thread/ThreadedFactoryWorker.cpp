/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "ThreadedFactoryWorker.h"
#include "core/base/ComponentFactory.h"

namespace core::thread
{


void ThreadedFactoryWorker::produce(base::ComponentFactory* factory)
{
  auto comp = factory->produce();
  created(comp);
}


}
