/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <QObject>

namespace core::base
{

class Component;
class ComponentFactory;

}

namespace core::thread
{


class ThreadedFactoryWorker :
    public QObject
{
  Q_OBJECT

signals:
  void created(base::Component*);

public:
  void produce(base::ComponentFactory*);
};


}
