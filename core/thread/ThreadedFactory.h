/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#pragma once

#include <QThread>
#include <optional>

namespace core::base
{

class Component;
class ComponentFactory;

}

namespace core::thread
{


class ThreadedFactory
{
public:
  static base::Component* produce(base::ComponentFactory&, const std::optional<QThread*>&);

private:
  static base::Component* produceNonThreaded(base::ComponentFactory&);
  static base::Component* produceThreaded(base::ComponentFactory&, QThread*);
};


}
