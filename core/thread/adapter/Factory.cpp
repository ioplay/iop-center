#include "Factory.h"

namespace core::thread::adapter
{


QThread* Factory::produce()
{
  auto thread = new QThread();
  threads.push_back(thread);
  thread->start();
  return thread;
}

void Factory::stop()
{
  for (const auto thread : threads) {
    thread->quit();
  }
  for (const auto thread : threads) {
    thread->wait();
  }
}


}
