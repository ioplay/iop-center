#pragma once

#include "core/thread/Thread.h"
#include <QList>

namespace core::thread::adapter
{


class Factory : //TODO rename to repository or so
    public Thread
{
public:
  QThread* produce() override;

  void stop();

private:
  QList<QThread*> threads{};

};


}
