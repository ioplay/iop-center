/*
 * (C) Copyright 2020 Urs Fässler
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

#include "core/main/ComponentControl.h"
#include "core/component/mpd/adapter/LocalSocketFactory.h"
#include "core/component/rfid/SpiRegisterDevice.h"
#include "core/component/rfid/adapter/SpiDev.h"
#include "core/main/adapter/LinuxSignalHandler.h"
#include "core/base/Configuration.h"
#include "core/setup/Builder.h"
#include "core/setup/Factories.h"
#include "core/setup/FactoryRepository.h"
#include "core/setup/fromCmdline.h"
#include "core/setup/registerFactories.h"
#include "core/thread/adapter/Factory.h"
#include "core/util/adapter/QtFilesystem.h"
#include "core/util/adapter/QtTimer.h"
#include "event/event.h"
#include <QCoreApplication>
#include <QDebug>
#include <QMetaType>
#include <iostream>

Q_DECLARE_METATYPE(event::SharedEvent)

namespace iop_center
{


int main(int argc, char** argv)
{
  qRegisterMetaType<event::SharedEvent>();

  QCoreApplication app{argc, argv};

  core::main::adapter::LinuxSignalHandler unixSignal{};

  core::component::mpd::adapter::LocalSocketFactory socketFactory{};
  core::util::adapter::QtFilesystem filesystem{};
  core::util::adapter::QtTimer timer{};
  core::component::rfid::adapter::SpiDev spidev{};
  core::component::rfid::SpiRegisterDevice mfrc522{spidev};
  const auto arguments = core::setup::parseCmdline(QCoreApplication::arguments(), filesystem);
  const core::base::Configuration configuration{filesystem, arguments.applicationName};

  core::main::ComponentControl phase{};
  core::thread::adapter::Factory threads{};

  {
    core::setup::FactoryRepository repo{};
    core::setup::Factories factories{std::cout, mfrc522, socketFactory, filesystem, timer, configuration};
    core::setup::registerFactories(repo, factories);
    core::setup::Builder builder{phase, repo, threads};
    core::setup::fromArguments(arguments, builder, filesystem);
  }

  QObject::connect(&unixSignal, &core::main::adapter::LinuxSignalHandler::shutdown, &phase, &core::main::ComponentControl::shutdown);
  QObject::connect(&phase, &core::main::ComponentControl::shutdownComplete, &app, &QCoreApplication::quit);

  phase.boot();
  app.exec();
  threads.stop();

  qDebug() << "bye";

  return EXIT_SUCCESS;
}


}


int main(int argc, char** argv)
{
  return iop_center::main(argc, argv);
}
