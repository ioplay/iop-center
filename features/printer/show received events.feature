# language: en

Feature: show received events
  As a software developer or operator
  I want to see some events
  In order to verify the functionality


Background:
  Given I start the application as "iop-center"
  And I load the component "Printer" as "printer"
  And I boot the application


Scenario: show binary.on events

  When I send to "printer" the event "switch.on" with the id "hello"

  Then I expect to see the cout:
    """
    event switch.on
    {
        "id": "hello"
    }

    """


Scenario: show binary.off events

  When I send to "printer" the event "switch.off" with the id "world"

  Then I expect to see the cout:
    """
    event switch.off
    {
        "id": "world"
    }

    """


Scenario: show player.play events

  When I send to "printer" the event "player.play" with the playlist:
    | name                                       |
    | file:///var/music/the artist/song1.ogg     |
    | file:///var/music/the artist/song3.mp3     |
    | file:///var/music/the artist/song4.ogg     |
    | file:///var/music/the artist/song2.ogg     |

  Then I expect to see the cout:
    """
    event player.play
    {
        "playlist": [
            "file:///var/music/the artist/song1.ogg",
            "file:///var/music/the artist/song3.mp3",
            "file:///var/music/the artist/song4.ogg",
            "file:///var/music/the artist/song2.ogg"
        ]
    }

    """
