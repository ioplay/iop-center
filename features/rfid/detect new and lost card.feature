# language: en

Feature: show detected card
  As a software developer
  I want to see the ID of a detected card
  In order to verify the RFID reader


Background:
  Given I start the application as "iop-center"
  And I load the component "Rfid" as "rfid"


Scenario: initialize the MFRC522

  When I boot the application

  Then I expect the following MFRC522 rx register values:
    | register | value |
    | 01       | 0F    |
    | 15       | 40    |
    | 14       | 83    |


Scenario: show the card ID when detected
  Given I boot the application
  And I clear the MFRC522 rx registers

  When I wait for 50 milliseconds

  Then I expect the following MFRC522 rx register values:
    | register | value |
    | 01       | 0C    |
    | 0A       | 80    |
    | 04       | 7f    |
    | 09       | 26    |
    | 0D       | 87    |

  Given the MFRC522 has the tx register values:
    | register | value |
    | 04       | 64    |
    | 06       | 00    |
    | 0A       | 02    |
    | 0C       | 10    |
    | 09       | 04 00 |
  And I clear the MFRC522 rx registers

  When I wait for 2 milliseconds

  Then I expect the following MFRC522 rx register values:
    | register | value |
    | 01       | 0C    |
    | 0A       | 80    |
    | 04       | 7f    |
    | 09       | 93 20 |
    | 0D       | 80    |

  Given the MFRC522 has the tx register values:
    | register | value |
    | 04       | 64    |
    | 06       | 00    |
    | 0A       | 05    |
    | 0C       | 10    |
    | 09       | 94 25 c0 1e 6f |

  When I wait for 2 milliseconds

  Then I expect from "rfid" the event "card.detected" with the content:
    """
    {
      "id": "9425c01e"
    }
    """

  When I clear all received events
  And I wait for 50 milliseconds

  Then I expect the following MFRC522 rx register values:
    | register | value |
    | 01       | 0C    |
    | 0A       | 80    |
    | 04       | 7f    |
    | 09       | 93 20 |
    | 0D       | 80    |

  Given the MFRC522 has the tx register values:
    | register | value |
    | 04       | 44    |
    | 06       | 00    |
    | 0A       | 00    |
    | 0C       | 10    |
    | 09       | 20    |

  When I wait for 2 milliseconds

  Then I expect from "rfid" the event "card.lost" with the content:
    """
    {
      "id": "9425c01e"
    }
    """
