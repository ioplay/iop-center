# language: en

Feature: error handling
  As a integrator
  I want the system to behave sane in the case of an error
  In order to have a stable and reliable system


Background:
  Given I start the application as "iop-center"
  And I load the component "Rfid" as "rfid"


Scenario: unable to open spi device
  Given the MFRC522 device can not be opened

  When I boot the application

  Then I expect the application to be shutdown
