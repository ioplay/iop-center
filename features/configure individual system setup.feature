# language: en

Feature: configure individual system setup
  As an integrator
  I want to specify my setup
  In order to control my individually combined hardware


Scenario: load custom setup
  Given I have the file "/tmp/data/my-custom-app.xml":
    """
    <iop-center>
      <thread id="printThread"/>
      <component class="StageNotification" id="stage"/>
      <component class="Printer" id="printer" thread="printThread"/>
      <connection from="stage" to="printer" />
    </iop-center>
    """

  When I start the application with the arguments:
    """
    /usr/bin/iop-center
    --setup=/tmp/data/my-custom-app.xml
    """
  And I boot the application

  Then I expect to see the cout:
    """
    event development.debug
    {
        "stage-change": "initialize"
    }
    event development.debug
    {
        "stage-change": "start"
    }

    """


Scenario: start as custom application
  Given I have the file "/usr/lib/my-custom-app/setup.xml":
    """
    <iop-center>
      <thread id="printThread"/>
      <component class="StageNotification" id="stage"/>
      <component class="Printer" id="printer" thread="printThread"/>
      <connection from="stage" to="printer" />
    </iop-center>
    """

  When I start the application with the arguments:
    """
    /usr/bin/my-custom-app
    """
  And I boot the application

  Then I expect to see the cout:
    """
    event development.debug
    {
        "stage-change": "initialize"
    }
    event development.debug
    {
        "stage-change": "start"
    }

    """


Scenario: start as application with different name
  Given I have the file "/usr/lib/my-custom-app/setup.xml":
    """
    <iop-center>
      <thread id="printThread"/>
      <component class="StageNotification" id="stage"/>
      <component class="Printer" id="printer" thread="printThread"/>
      <connection from="stage" to="printer" />
    </iop-center>
    """

  When I start the application with the arguments:
    """
    /usr/bin/iop-center
    --name=my-custom-app
    """
  And I boot the application

  Then I expect to see the cout:
    """
    event development.debug
    {
        "stage-change": "initialize"
    }
    event development.debug
    {
        "stage-change": "start"
    }

    """


Scenario: read configuration from directory that matches the application and component name
  Given I have the file "/var/lib/hello/world/settings":
    """
    started-file=file:///var/jingle/boot.ogg
    """
  And I have the files:
    | filename                            |
    | /var/jingle/boot.ogg                |
  And I start the application as "hello"
  And I load the component "Matchplay" as "world"

  When I boot the application

  Then I expect from "world" the event "player.play" with the content:
    """
    {
      "playlist": [
        "file:///var/jingle/boot.ogg"
      ]
    }
    """
