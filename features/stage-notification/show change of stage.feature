# language: en

Feature: show change of stage
  As a software developer or operator
  I want to see when the system changes the stage
  In order to verify the functionality


Background:
  Given I start the application as "iop-center"


Scenario: show boot stages
  Given I load the component "StageNotification" as "testee"

  When I boot the application

  Then I expect from "testee" the event "development.debug" with the content:
    """
    {
      "stage-change": "initialize"
    }
    """
  And I expect from "testee" the event "development.debug" with the content:
    """
    {
      "stage-change": "start"
    }
    """
  And I expect no events from "testee"


Scenario: show shutdown stages
  Given I load the component "StageNotification" as "testee"
  And I boot the application
  And I clear all received events

  When I shutdown the application

  Then I expect from "testee" the event "development.debug" with the content:
    """
    {
      "stage-change": "stop"
    }
    """
  And I expect from "testee" the event "development.debug" with the content:
    """
    {
      "stage-change": "deinitialize"
    }
    """
  And I expect no events from "testee"
