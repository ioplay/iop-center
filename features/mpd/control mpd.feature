# language: en

Feature: control mpd
  As an operator
  I want to use mpd
  In order to play a music files


Background:
  Given I start the application as "iop-center"
  And I load the component "Mpd" as "mpd"
  And I start the application boot process
  And I receive the following content on "/var/run/mpd/socket":
    """
    OK MPD 0.21.11

    """
  And I clear the content of "/var/run/mpd/socket"
  And I wait until the application boot process is finished


Scenario: replace and start the new playlist on the play event

  When I send to "mpd" the event "player.play" with the playlist:
    | name                                       |
    | file:///var/music/the artist/song1.ogg     |
    | file:///var/music/the artist/song3.mp3     |
    | file:///var/music/the artist/song4.ogg     |
    | file:///var/music/the artist/song2.ogg     |
  Then I expect the following content on "/var/run/mpd/socket":
    """
    noidle

    """

  When I receive the following content on "/var/run/mpd/socket":
    """
    OK

    """
  Then I expect the following content on "/var/run/mpd/socket":
    """
    command_list_begin
    clear
    add "file:///var/music/the artist/song1.ogg"
    add "file:///var/music/the artist/song3.mp3"
    add "file:///var/music/the artist/song4.ogg"
    add "file:///var/music/the artist/song2.ogg"
    play
    command_list_end

    """

  When I receive the following content on "/var/run/mpd/socket":
    """
    OK

    """
  Then I expect the following content on "/var/run/mpd/socket":
    """
    idle

    """


Scenario: does not add nor start playing for an empty playlist

  When I send to "mpd" the event "player.play" with the playlist:
    | name                                       |

  Then I expect the following content on "/var/run/mpd/socket":
    """
    noidle

    """

  When I receive the following content on "/var/run/mpd/socket":
    """
    OK

    """
  Then I expect the following content on "/var/run/mpd/socket":
    """
    clear

    """

  When I receive the following content on "/var/run/mpd/socket":
    """
    OK

    """
  Then I expect the following content on "/var/run/mpd/socket":
    """
    idle

    """


Scenario: pause on the pause event

  When I send to "mpd" the event "player.pause"

  Then I expect the following content on "/var/run/mpd/socket":
    """
    noidle

    """
  When I receive the following content on "/var/run/mpd/socket":
    """
    OK

    """

  Then I expect the following content on "/var/run/mpd/socket":
    """
    pause 1

    """

  When I receive the following content on "/var/run/mpd/socket":
    """
    OK

    """
  Then I expect the following content on "/var/run/mpd/socket":
    """
    idle

    """


Scenario: continues on the continue event

  When I send to "mpd" the event "player.continue"

  Then I expect the following content on "/var/run/mpd/socket":
    """
    noidle

    """
  When I receive the following content on "/var/run/mpd/socket":
    """
    OK

    """

  Then I expect the following content on "/var/run/mpd/socket":
    """
    pause 0

    """

  When I receive the following content on "/var/run/mpd/socket":
    """
    OK

    """
  Then I expect the following content on "/var/run/mpd/socket":
    """
    idle

    """


Scenario: goes back to idle after a status update

  When I receive the following content on "/var/run/mpd/socket":
    """
    changed: player
    OK

    """
  Then I expect the following content on "/var/run/mpd/socket":
    """
    idle

    """

