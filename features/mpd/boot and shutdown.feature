# language: en

Feature: mpd boot and shutdown
  As an operator
  I want to have a stable connection to mpd
  In order to have a fast response to user input


Background:
  Given I start the application as "iop-center"
  And I load the component "Mpd" as "mpd"


Scenario: don't send idle request when mpd is not ready

  When I start the application boot process

  Then I expect the following content on "/var/run/mpd/socket":
    """

    """


Scenario: go into idle mode when booted
  Given I start the application boot process

  When I receive the following content on "/var/run/mpd/socket":
    """
    OK MPD 0.21.11

    """

  Then I expect the following content on "/var/run/mpd/socket":
    """
    idle

    """
