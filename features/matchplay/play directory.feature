# language: en

Feature: play multiple songs with a RFID card
  As a child
  I want to start the music with a RFID card
  In order to listen to a specific album


Background:
  Given I have the file "/var/lib/iop-center/matchplay/matching":
    """
    12345678=music/the artist/
    9abcdef0=file:///var/AnotherArtist/
    00112233=file:///var/music/empty/
    44556677=file:///var/invalid-directory/
    """
  And I have the file "/var/lib/iop-center/matchplay/settings":
    """
    root-directory=file:///var/
    """
  And I have the files:
    | filename                        |
    | /var/music/the artist/song1.ogg |
    | /var/music/the artist/song3.mp3 |
    | /var/music/the artist/song2.ogg |
    | /var/music/the artist/song4.ogg |
    | /var/music/the artist/hello.txt |
    | /var/AnotherArtist/c.ogg        |
    | /var/AnotherArtist/a.ogg        |
    | /var/AnotherArtist/b.mp3        |
    | /var/music/empty/               |
  And I start the application as "iop-center"
  And I load the component "Matchplay" as "matchplay"
  And I boot the application


Scenario: start playing a directory when a card is detected

  When I send to "matchplay" the event "switch.on" with the id "12345678"

  Then I expect from "matchplay" the event "player.play" with the content:
    """
    {
      "playlist": [
        "file:///var/music/the artist/song1.ogg",
        "file:///var/music/the artist/song2.ogg",
        "file:///var/music/the artist/song3.mp3",
        "file:///var/music/the artist/song4.ogg"
      ]
    }
    """

Scenario: start playing a different directory when a new card is detected
  Given I send to "matchplay" the event "switch.on" with the id "12345678"
  And I send to "matchplay" the event "switch.off" with the id "12345678"
  And I clear all received events

  When I send to "matchplay" the event "switch.on" with the id "9abcdef0"

  Then I expect from "matchplay" the event "player.play" with the content:
    """
    {
      "playlist": [
        "file:///var/AnotherArtist/a.ogg",
        "file:///var/AnotherArtist/b.mp3",
        "file:///var/AnotherArtist/c.ogg"
      ]
    }
    """


Scenario: start playing a different directory when a new switch on event is received
  Given I send to "matchplay" the event "switch.on" with the id "12345678"
  And I clear all received events

  When I send to "matchplay" the event "switch.on" with the id "9abcdef0"

  Then I expect from "matchplay" the event "player.play" with the content:
    """
    {
      "playlist": [
        "file:///var/AnotherArtist/a.ogg",
        "file:///var/AnotherArtist/b.mp3",
        "file:///var/AnotherArtist/c.ogg"
      ]
    }
    """


Scenario: does not stop playing when a switch off event is received with a different id than actual playing
  Given I send to "matchplay" the event "switch.on" with the id "12345678"
  And I send to "matchplay" the event "switch.on" with the id "9abcdef0"
  And I clear all received events

  When I send to "matchplay" the event "switch.off" with the id "12345678"

  Then I expect no events from "matchplay"


Scenario: does stop playing when a switch off event is received with the id that is playing
  Given I send to "matchplay" the event "switch.on" with the id "12345678"
  And I send to "matchplay" the event "switch.on" with the id "9abcdef0"
  And I send to "matchplay" the event "switch.off" with the id "12345678"
  And I clear all received events

  When I send to "matchplay" the event "switch.off" with the id "9abcdef0"

  Then I expect from "matchplay" the event "player.pause"
