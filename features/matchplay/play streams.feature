# language: en

Feature: play streams
  As a user
  I want to play web streams
  In order to listen to radio


Background:
  Given I have the file "/var/lib/iop-center/matchplay/matching":
    """
    abcd=https://stream.radioparadise.com/rp_192m.ogg
    """
  And I start the application as "iop-center"
  And I load the component "Matchplay" as "matchplay"
  And I boot the application


Scenario: start stream on a matching event

  When I send to "matchplay" the event "switch.on" with the id "abcd"

  Then I expect from "matchplay" the event "player.play" with the content:
    """
    {
      "playlist": [
        "https://stream.radioparadise.com/rp_192m.ogg"
      ]
    }
    """
