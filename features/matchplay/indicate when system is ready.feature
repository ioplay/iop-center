# language: en

Feature: indicate when system is ready
  As a parent
  I want to knwo when the system is ready
  In order to give it to the child


Background:
  Given I start the application as "iop-center"


Scenario: start playing a jingle when the system is ready
  Given I have the file "/var/lib/iop-center/matchplay/settings":
    """
    started-file=file:///var/jingle/boot.ogg
    """
  And I have the files:
    | filename                            |
    | /var/jingle/boot.ogg                |
  And I load the component "Matchplay" as "matchplay"

  When I boot the application

  Then I expect from "matchplay" the event "player.play" with the content:
    """
    {
      "playlist": [
        "file:///var/jingle/boot.ogg"
      ]
    }
    """


Scenario: does nothing when no started jingle is defined
  Given I load the component "Matchplay" as "matchplay"

  When I boot the application

  Then I expect no events from "matchplay"


Scenario: does nothing when jingle does not exist
  Given I have the file "/var/lib/iop-center/settings":
    """
    started-file=file:///var/jingle/boot.ogg
    """
  And I load the component "Matchplay" as "matchplay"

  When I boot the application

  Then I expect no events from "matchplay"
