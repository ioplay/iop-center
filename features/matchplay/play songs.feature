# language: en

Feature: play songs with RFID card
  As a child
  I want to start a song with a RFID card
  In order to listen to a specific song


Background:
  Given I have the file "/var/lib/iop-center/matchplay/matching":
    """
    12345678=file:///var/music/the artist/Song Nr 1.mp3
    9abcdef0=file:///var/äéúóð.ogg
    not-here=file:///var/not-here.ogg
    """
  And I have the files:
    | filename                            |
    | /var/music/the artist/Song Nr 1.mp3 |
    | /var/äéúóð.ogg                      |
  And I start the application as "iop-center"
  And I load the component "Matchplay" as "matchplay"
  And I boot the application


Scenario: start playing a song when a card is detected

  When I send to "matchplay" the event "switch.on" with the id "12345678"

  Then I expect from "matchplay" the event "player.play" with the content:
    """
    {
      "playlist": [
        "file:///var/music/the artist/Song Nr 1.mp3"
      ]
    }
    """


Scenario: stop playing when card is lost
  Given I send to "matchplay" the event "switch.on" with the id "12345678"
  And I clear all received events

  When I send to "matchplay" the event "switch.off" with the id "12345678"

  Then I expect from "matchplay" the event "player.pause"


Scenario: start playing a different song when a new card is detected
  Given I send to "matchplay" the event "switch.on" with the id "12345678"
  And I send to "matchplay" the event "switch.off" with the id "12345678"
  And I clear all received events

  When I send to "matchplay" the event "switch.on" with the id "9abcdef0"

  Then I expect from "matchplay" the event "player.play" with the content:
    """
    {
      "playlist": [
        "file:///var/äéúóð.ogg"
      ]
    }
    """


Scenario: continues playing same song when same card is detected as before
  Given I send to "matchplay" the event "switch.on" with the id "12345678"
  And I send to "matchplay" the event "switch.off" with the id "12345678"
  And I clear all received events

  When I send to "matchplay" the event "switch.on" with the id "12345678"

  Then I expect from "matchplay" the event "player.continue"


Scenario: does not add file that does not exists

  When I send to "matchplay" the event "switch.on" with the id "not-here"

  Then I expect from "matchplay" the event "player.play" with the content:
    """
    {
      "playlist": [
      ]
    }
    """
