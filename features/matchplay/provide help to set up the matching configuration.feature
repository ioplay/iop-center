# language: en

Feature: provide help to set up the matching configuration
  As a operator
  I want to easily configure the matching component
  In order to connect events with files


Background:
  Given I have the file "/var/lib/iop-center/matchplay/matching":
    """
    12345678=artist3/
    """
  And I have the file "/var/lib/iop-center/matchplay/settings":
    """
    root-directory=file:///var/music/
    """
  And I have the files:
    | filename                              |
    | /var/music/artist1/song1.ogg          |
    | /var/music/artist1/song2.ogg          |
    | /var/music/artist2/song1.mpeg         |
    | /var/music/artist3/song4.ogg          |
    | /var/music/artist4/album1/            |
    | /var/music/artist5/                   |
    | /var/music/artist6/album1/song1.mp3   |
    | /var/music/artist6/album1/song2.mp3   |
    | /var/music/artist6/album2/song1.mp3   |
    | /var/music/artist1/.meta/song1.ogg    |
  And I start the application as "iop-center"
  And I load the component "Matchplay" as "matchplay"


Scenario: show directories with music files

  When I boot the application

  Then I expect to see the cout:
    """
    possible entries for matching configuration in /var/lib/iop-center/matchplay/:
      artist1/
      artist6/album1/
      artist6/album2/

    """
